package The_eight_week;

/**
 * HeapSort sorts a given array of Comparable objects using a heap.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class HeapSort<T>
{
    /**
     * Sorts the specified array using a Heap
	 *
	 * @param data the data to be added to the heapsort
     */
	public void HeapSort(T[] data) 
	{
		ArrayHeap<T> temp = new ArrayHeap<T>();

		// copy the array into a heap 
		for (int i = 0; i < data.length; i++)
		    temp.addElement(data[i]);

		// place the sorted elements back into the array 
		int count = 0;
		while (!(temp.isEmpty()))
		{
			data[count] = temp.removeMin();
			count++;
		}
	}
	public void BigHeapSort(T[] data)
	{
		BigArrayHeap<T> temp = new BigArrayHeap<T>();

		// copy the array into a heap
		for (int i = 0; i < data.length; i++) {
			temp.addElement(data[i]);
			;
		}

		// place the sorted elements back into the array
		int count = 0;
		while (!(temp.isEmpty()))
		{
			data[count] = temp.removeMax();
			count++;
			System.out.print(count+": ");
			for (int n = 0;n<data.length;n++)
				System.out.print(data[n]+" ");
			System.out.println();
		}
	}
}


