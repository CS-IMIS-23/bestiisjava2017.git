package The_eight_week;

public class PriorityQueueTest {
    public static void main(String[] args) {
        String res = "";
        PriorityQueue queue = new PriorityQueue();
        queue.addElement(0,1);
        queue.addElement(5,2);
        queue.addElement(8,3);
        queue.addElement(4,4);
        queue.addElement(10,5);
        queue.addElement(9,6);

        for(int i = 0;i<queue.getNum();i++)
            res += queue.removeNext()+ " ";
        System.out.println(res);
    }
}
