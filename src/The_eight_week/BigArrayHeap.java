package The_eight_week;

import The_two_week.EmptyCollectionException;

public class BigArrayHeap<T> extends ArrayBinaryTree<T> implements BigHeapADT<T> {
    /**
     * Creates an empty heap.
     */
    public BigArrayHeap() {
        super();
    }

    /**
     * Adds the specified element to this heap in the appropriate
     * position according to its key value.
     *
     * @param obj the element to be added to the heap
     */
    public void addElement(T obj) {
        if (count == tree.length)
            expandCapacity();

        tree[count] = obj;
        count++;
        modCount++;
//        for (int i = 0; i < tree.length && tree[i] != null; i++)
//            System.out.print(tree[i] + " ");
//        System.out.println();

        if (count > 1)
            heapifyAdd();
        for (int i = 0; i < tree.length && tree[i] != null; i++)
            System.out.print(tree[i] + " ");
        System.out.println();
    }

    public void add(T obj) {
        if (count == tree.length)
            expandCapacity();

        tree[count] = obj;
        count++;
        modCount++;
//        for (int i = 0; i < tree.length && tree[i] != null; i++)
//            System.out.print(tree[i] + " ");
//        System.out.println();

        if (count > 1)
            heapifyAdd();
//        for (int i = 0; i < tree.length && tree[i] != null; i++)
//            System.out.print(tree[i] + " ");
//        System.out.println();
    }

    /**
     * Reorders this heap to maintain the ordering property after
     * adding a node.
     */
    private void heapifyAdd() {
        T temp;
        int next = count - 1;

        temp = tree[next];

        while ((next != 0) &&
                (((Comparable) temp).compareTo(tree[(next - 1) / 2]) > 0)) {

            tree[next] = tree[(next - 1) / 2];
            next = (next - 1) / 2;
        }
        tree[next] = temp;
    }

    public void heapify() {
        BigArrayHeap<T> temp = new BigArrayHeap<T>();
//
        // copy the array into a heap
        for (int i = 0; i < tree.length && tree[i] != null; i++) {
            temp.add(tree[i]);
        }

        // place the sorted elements back into the array

        T add;
        int next = count - 1;

        add = tree[next];

        while ((next != 0) &&
                (((Comparable) add).compareTo(tree[(next - 1) / 2]) > 0)) {

            tree[next] = tree[(next - 1) / 2];
            next = (next - 1) / 2;
        }
        tree[next] = add;

        int count = 0;
        while (!(temp.isEmpty())) {
            tree[count] = temp.removeMax();
            count++;
            System.out.print(count + ": ");
//            System.out.print(temp+" ");
            for (int n = count - 1; n >= 0; n--)
                System.out.print(temp + " ");
            System.out.println();
        }
    }

    /**
     * Remove the element with the lowest value in this heap and
     * returns a reference to it. Throws an EmptyCollectionException if
     * the heap is empty.
     *
     * @return a reference to the element with the lowest value in this heap
     * @throws EmptyCollectionException if the heap is empty
     */
    public T removeMax() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("ArrayHeap");

        T maxElement = tree[0];
        tree[0] = tree[count - 1];
        tree[count - 1] = maxElement;
        heapifyRemove();
//        tree[count-1] = maxElement;
        count--;
        modCount--;

        return maxElement;
    }

    /**
     * Reorders this heap to maintain the ordering property
     * after the minimum element has been removed.
     */
    private void heapifyRemove() {
        T temp;
        int node = 0;
        int left = 1;
        int right = 2;
        int next;

        if ((tree[left] == null) && (tree[right] == null))
            next = count;
        else if (tree[right] == null)
            next = left;
        else if (((Comparable) tree[left]).compareTo(tree[right]) > 0)
            next = left;
        else
            next = right;
        temp = tree[node];

        while ((next < count) &&
                (((Comparable) tree[next]).compareTo(temp) > 0)) {
            tree[node] = tree[next];
            node = next;
            left = 2 * node + 1;
            right = 2 * (node + 1);
            if ((tree[left] == null) && (tree[right] == null))
                next = count;
            else if (tree[right] == null)
                next = left;
            else if (((Comparable) tree[left]).compareTo(tree[right]) > 0)
                next = left;
            else
                next = right;
        }
        tree[node] = temp;
    }

    /**
     * Returns the element with the lowest value in this heap.
     * Throws an EmptyCollectionException if the heap is empty.
     *
     * @return the element with the lowest value in this heap
     * @throws EmptyCollectionException if the heap is empty
     */
    public T findMax() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("ArrayHeap");

        return tree[0];
    }

    public void sort() {
        int i = count;
        while (i > 0) {
            removeMax();
            i--;
            System.out.println(toString());
        }

    }

    public String toString() {
        String str = "";
        int i = 0;
        while (i < 8) {
            str += tree[i] + " ";
            i++;
        }
        return str;
    }

}