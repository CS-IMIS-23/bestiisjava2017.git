package The_eight_week;

import java.util.Iterator;
import java.util.StringTokenizer;

public class Test {
    public static void main(String[] args) {
        BigArrayHeap big = new BigArrayHeap();
        String res = "";
        big.addElement(36);
        big.addElement(30);
        big.addElement(18);
        big.addElement(40);
        big.addElement(32);
        big.addElement(45);
        big.addElement(22);
        big.addElement(50);
        Iterator i = big.iteratorLevelOrder();
        while (i.hasNext())
            res += i.next() + " ";
        System.out.println("初始大顶堆: "+res);

//        big.heapify();
        big.sort();

    }
}
