package Eleven_week;

public class DVD1 {
    private String name;

    //-----------------------------------------------------------------
    //  Sets up the new magazine with its title.
    //-----------------------------------------------------------------
    public DVD1(String newTitle) {
        name = newTitle;
    }

    //-----------------------------------------------------------------
    //  Returns this magazine as a string.
    //-----------------------------------------------------------------
    public String toString() {
        return name;
    }
}
