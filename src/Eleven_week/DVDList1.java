package Eleven_week;

import java.util.ArrayList;

public class DVDList1 {
    private DVDNode list;
    ArrayList<DVD1> ma = new ArrayList<DVD1>();

    public DVDList1() {
        list = null;
    }

    public void add(DVD1 dvd) {
        DVDNode node = new DVDNode(dvd);
        DVDNode current;
        int index = 0;
        if (list == null) {
            list = node;
        } else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public String toString() {
        String result = "";

        DVDNode current = list;

        while (current != null) {
            result += current.dvd + "\n";
            current = current.next;
        }

        return result;
    }

    private class DVDNode {
        public DVD1 dvd;
        public DVDNode next;

        public DVDNode(DVD1 d) {
            dvd = d;
            next = null;
        }
    }
}
