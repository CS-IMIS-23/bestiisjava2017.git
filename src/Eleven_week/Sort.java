package Eleven_week;

import java.util.ArrayList;

public class Sort {
    private NumNode list;
    ArrayList<Integer> ma = new ArrayList<Integer>();

    public Sort() {
        list = null;
    }

    public void add(int a) {
        NumNode node = new NumNode(a);
        NumNode current;
        if (list == null) {
            list = node;
        } else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public NumNode selectSortList()
    {
        NumNode head = list;
        if(head==null)
        {
            return head;
        }
        NumNode q=head;
        NumNode p=head.next;
        int tmp=0;
        while(head!=null)
        {
            while(p!=null)
            {
                if(p.num<head.num)
                {
                    tmp=head.num;
                    head.num=p.num;
                    p.num=tmp;
                }
                p=p.next;
            }
            head=head.next;
            p=head;
        }
        return q;
    }


    public String toString() {
        String result = "";

        NumNode current = list;

        while (current != null) {
            result += current.num + "\n";
            current = current.next;
        }

        return result;
    }

    private class NumNode {
        public int num;
        public NumNode next;

        public NumNode(int a) {
            num = a;
            next = null;
        }
    }
}
