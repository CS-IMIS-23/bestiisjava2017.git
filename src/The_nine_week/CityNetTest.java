package The_nine_week;

public class CityNetTest {
    public static void main(String[] args) {
        CreatNet city = new CreatNet();
        city.addVertex("北京");
        city.addVertex("上海");
        city.addVertex("广州");
        city.addVertex("海南");
        city.addVertex("香港");
        city.addEdge("北京", "上海", 200);
        city.addEdge("北京", "海南", 50);
        city.addEdge("海南", "上海", 100);
        city.addEdge("广州", "上海", 250);
        System.out.println(city.toString());
        System.out.println(city.shortestPathLength("香港", "上海"));
        System.out.println(city.shortestPathLength("北京", "上海"));
//        if (city.shortestPathLength("北京", "上海") == 0)
//            System.out.println("No pass");
//        else
//            System.out.println(city.shortestPathLength("北京", "上海"));
//        if (city.shortestPathLength("北京", "香港") == 0)
//            System.out.println("No pass");
//        else
//            System.out.println(city.shortestPathLength("北京", "香港"));
        System.out.println(city.shortestPathWeight("北京", "上海"));
        System.out.println(city.shortestPathWeight("北京", "香港"));

    }
}
