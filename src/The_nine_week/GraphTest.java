package The_nine_week;

import java.util.Iterator;

public class GraphTest {
    public static void main(String[] args) {
//        Graph graph = new Graph();
//        graph.addVertex(5);
//        graph.addVertex(3);
//        graph.addEdge(0, 1);
//        System.out.println(graph.toString());

        GraphLinked gl = new GraphLinked();
        System.out.println(gl.size());
        gl.addVertex(5);
        gl.addVertex(8);
        gl.addVertex(4);
        gl.addVertex(9);
        gl.addVertex(1);
        gl.addVertex(3);
        gl.addVertex(2);
        gl.addEdge(5, 4);
        gl.addEdge(4, 9);
//        gl.addEdge(4, 9);
        gl.addEdge(3, 9);
        gl.addEdge(4, 2);
//        gl.removeVertex(9);
        System.out.println("初始：");
        gl.printNodeList();
        Iterator it = gl.iteratorBFS(3);
        while (it.hasNext())
            System.out.print(it.next() + " ");
        System.out.println();
        Iterator its = gl.iteratorDFS(3);
        while (its.hasNext())
            System.out.print(its.next() + " ");
        System.out.println();
        System.out.println(gl.isConnected());
        Iterator itss = gl.iteratorShortestPath(5,3);
        while (itss.hasNext())
            System.out.print(itss.next() + " ");
    }
}
//        System.out.println(gl.size());
//        System.out.println(gl.isEmpty());
//        gl.removeEdge(3,9);
//        gl.removeVertex(5);
//        System.out.println("删除：");
//        gl.printNodeList();
//        System.out.println(gl.size());
//        System.out.println(gl.isEmpty());
//        gl.removeVertex(2);
//        gl.printNodeList();
//        System.out.println(gl.size());
//        System.out.println(gl.isEmpty());

