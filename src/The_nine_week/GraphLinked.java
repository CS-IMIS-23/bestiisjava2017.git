package The_nine_week;

import The_six_week.ArrayUnorderedList;
import The_six_week.UnorderedListADT;
import The_three_week.LinkedQueue;
import The_three_week.LinkedStack;
import The_three_week.QueueADT;
import The_two_week.StackADT;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GraphLinked<T> implements GraphADT<T> {
    // 节点链表list，该list的每一项又是一个ArrayList,即每个节点相当于一个二维数组
    private ArrayList<Node> nodeList;
    private int maxCount;
    private int modCount;
    protected T[] vertices;
    private int Int;

    // 初始化链表
    public GraphLinked() {
        maxCount = 0;
        modCount = 0;
        Int = 0;
        // 初始化大小为maxCount的链表list
        nodeList = new ArrayList<Node>();
    }

    @Override
    public void addVertex(T vertex) {
        Node a = new Node(vertex);
        nodeList.add(a);
        maxCount++;
        modCount++;
    }

    @Override
    public void removeVertex(T vertex) {
        int i = 0;
        while (nodeList.get(i).getElement() != vertex) {
            i++;
        }
        nodeList.remove(i);
        maxCount--;
        modCount++;
        for (int a = 0; a < maxCount; a++) {
            Node temp = nodeList.get(a);
            while (temp.getNext() != null) {
                if (temp.getNext().getElement() == vertex)
                    temp.setNext(temp.getNext().getNext());
                temp = temp.getNext();
            }
            break;
        }
    }

    @Override
    public void addEdge(T vertex1, T vertex2) {
        int i = 0;
        while (nodeList.get(i).getElement() != vertex1) {
            i++;
        }
        Node temp = nodeList.get(i);
        while (temp.getNext() != null) {
            temp = temp.getNext();
        }
        temp.setNext(new Node(vertex2));
        int j = 0;
        while (nodeList.get(j).getElement() != vertex2) {
            j++;
        }
        Node temp1 = nodeList.get(j);
        while (temp1.getNext() != null) {
            temp1 = temp1.getNext();
        }
        temp1.setNext(new Node(vertex1));

    }

    @Override
    public void removeEdge(T vertex1, T vertex2) {
        int i = 0;
        while (nodeList.get(i).getElement() != vertex1) {
            i++;
        }
        Node temp = nodeList.get(i);
        while (temp.getNext().getElement() != vertex2) {
            temp = temp.getNext();
        }
        if (temp.getNext().getNext() == null) {
            temp.setNext(null);
        } else {
            temp.getNext().setNext(temp.getNext().getNext());
        }
    }

    protected boolean indexIsValid(int index) {
        if (index < size())
            return true;
        else
            return false;
    }

    @Override
    public Iterator iteratorBFS(T startVertex) {
        return iteratorBFS(getIndex(startVertex));
    }


    private Iterator<T> iteratorBFS(int startIndex) {
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[maxCount];
        for (int i = 0; i < maxCount; i++)
            visited[i] = false;

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToRear((T) nodeList.get(x).getElement());
//            Node temp = nodeList.get(x);
            for (int i = 0; i < maxCount; i++) {
                if (hasEdge(x, i) && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                    Int++;
                }
//                temp = temp.getNext();
            }
        }
        for (int i = 0; i < maxCount; i++) {
            if (visited[i] == false)
                resultList.addToRear((T) nodeList.get(i).getElement());
        }
        return new GraphIterator(resultList.iterator());
    }

    public boolean hasEdge(int a, int b) {
        if (a == b)
            return false;
        Node vertex1 = nodeList.get(a);
        Node vertex2 = nodeList.get(b);
        while (vertex1 != null) {
            if (vertex1.getElement() == vertex2.getElement())
                return true;
            vertex1 = vertex1.getNext();
        }
        return false;
    }

    protected class NetworkIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        public NetworkIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    //根据顶点元素得到该索引值
    public int getIndex(T vertex) {
        int i = 0;
        while (i < maxCount) {
            if (nodeList.get(i).getElement() != vertex) {
                i++;
            } else
                break;
        }
        return i;
    }

    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    private Iterator<T> iteratorShortestPath(int startIndex, int targetIndex)
    {
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return resultList.iterator();

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);
        while (it.hasNext())
            resultList.addToRear((T)nodeList.get(((Integer)it.next())).getElement());
        return new GraphIterator(resultList.iterator());
    }


    private Iterator<Integer> iteratorShortestPathIndices
            (int startIndex, int targetIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[maxCount];
        int[] predecessor = new int[maxCount];
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList =
                new ArrayUnorderedList<Integer>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                (startIndex == targetIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[maxCount];
        for (int i = 0; i < maxCount; i++)
            visited[i] = false;

        traversalQueue.enqueue(Integer.valueOf(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex))
        {
            index = (traversalQueue.dequeue()).intValue();

            //Update the pathLength for each unvisited vertex adjacent
            //     to the vertex at the current index.
            for (int i = 0; i < maxCount; i++)
            {
                if (hasEdge(index,i) && !visited[i])
                {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(Integer.valueOf(i));
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex)  // no path must have been found
            return resultList.iterator();

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(Integer.valueOf(index));
        do
        {
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty())
            resultList.addToRear(((Integer)stack.pop()));

        return new GraphIndexIterator(resultList.iterator());
    }



    @Override
    public Iterator iteratorDFS(T startVertex) {
        return iteratorDFS(getIndex(startVertex));

    }

    public Iterator<T> iteratorDFS(int startIndex) {
        Integer x;
        boolean found;
        T[] vertices = (T[]) (new Object[maxCount]);
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[maxCount];

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        for (int i = 0; i < maxCount; i++)
            visited[i] = false;

        traversalStack.push(startIndex);
        resultList.addToRear((T) nodeList.get(startIndex).getElement());
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            //Find a vertex adjacent to x that has not been visited
            //     and push it on the stack
            for (int i = 0; (i < maxCount) && !found; i++) {
                if (hasEdge(x, i) && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addToRear((T) nodeList.get(i).getElement());
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty())
                traversalStack.pop();
        }
        for (int i = 0; i < maxCount; i++) {
            if (visited[i] == false)
                resultList.addToRear((T) nodeList.get(i).getElement());
        }
        return new GraphIterator(resultList.iterator());
    }


    @Override
    public boolean isEmpty() {
        if (size() != 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean isConnected() {
        boolean result = true;
        for(int i=0;i<maxCount;i++){
            int temp=0;
            temp=getSizeOfIterator(iteratorBFS(i));
            if(temp!=Int)
            {
                result = false;
                break;
            }
        }
        return result;
    }

    private int getSizeOfIterator(Iterator iterator) {
        int size = 0;
        while(iterator.hasNext()){
            size++;
            iterator.next();
        }
        return size;
    }

    @Override
    public int size() {
        return maxCount;
    }

    // 输出链表图
    public void printNodeList() {
        // 遍历每个顶点
        for (int i = 0; i < nodeList.size(); i++) {
            // 输出当前顶点编号

            // 获取当前节点下的所有节点
            Node nodes = nodeList.get(i);
            System.out.print("顶点：" + nodeList.get(i).getElement());

            // 遍历当前节点下所有的节点
            while (nodes.getNext() != null) {
                System.out.print(" --> " + nodes.getNext().getElement());
                nodes = nodes.getNext();
            }
//            for (int j = 0; j < nodes.size(); j++) {
//                System.out.print(" --> " + nodes.get(j).getElement());
//            }

            // 该链表list遍历完成后回车继续下一个链表
            System.out.println(";");
        }
    }


    protected class GraphIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public GraphIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Inner class to represent an iterator over the indexes of this graph
     */
    protected class GraphIndexIterator implements Iterator<Integer> {
        private int expectedModCount;
        private Iterator<Integer> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public GraphIndexIterator(Iterator<Integer> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public Integer next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}



