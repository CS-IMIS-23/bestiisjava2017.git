package The_nine_week;

import The_six_week.ArrayUnorderedList;
import The_six_week.UnorderedListADT;
import The_three_week.LinkedQueue;
import The_three_week.LinkedStack;
import The_three_week.QueueADT;
import The_two_week.StackADT;

import java.util.*;

public class CreatNet<T> implements NetworkADT<T> {
    protected final int DEFAULT_CAPACITY = 5;
    protected int numVertices;    // //个数
    protected Double[][] adjMatrix;    //邻接矩阵（存价格）
    protected T[] vertices;    // （存顶点）
    protected int modCount;     //操作次数

    public CreatNet() {
        numVertices = 0;
        this.adjMatrix = new Double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    //添加顶点
    @Override
    public void addVertex(T vertex) {
        //越界扩容
        if ((numVertices + 1) == adjMatrix.length)
            expandCapacity();

        //把新加顶点到剩下所有顶点的路径定义为正无穷
        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
            adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
        }
        numVertices++;
        modCount++;
    }

    //删除顶点
    @Override
    //根据顶点元素删除
    public void removeVertex(T vertex) {
        removeVertex(getIndex(vertex));
    }


    //根据索引去找到该顶点
    public void removeVertex(int index) {
        //当索引值存在时
        if (indexIsValid(index)) {
            //从该顶点开始，顶点集中的每个数需要向前移一位
            for (int j = index; j < numVertices - 1; j++) {
                vertices[j] = vertices[j + 1];
            }
            vertices[numVertices - 1] = null;

            //从该顶点开始，边的起点从该顶点变为下一个顶点依次类推
            for (int i = index; i < numVertices - 1; i++) {
                for (int x = 0; x < numVertices; x++)
                    adjMatrix[i][x] = adjMatrix[i + 1][x];

            }
            //因为无向图，所以从该顶点开始，边的终点从该顶点变为下一个顶点依次类推
            for (int i = index; i < numVertices; i++) {
                for (int x = 0; x < numVertices; x++)
                    adjMatrix[x][i] = adjMatrix[x][i + 1];
            }
            //重新定义最后一个数到各个顶点的边为空
            for (int i = 0; i < numVertices; i++) {
                adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
                adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
            }
            numVertices--;
            modCount++;
        }
    }

    @Override
    public void addEdge(T vertex1, T vertex2) {
        addEdge(getIndex(vertex1), getIndex(vertex2));
    }

    private void addEdge(int index1, int index2) {
        //当起点终点的索引值都存在的时候，因为是无向图，所以权重都相同
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
            adjMatrix[index2][index1] = Double.POSITIVE_INFINITY;
            modCount++;
        }
    }

    @Override
    //根据顶点元素添加边
    public void addEdge(T vertex1, T vertex2, double weight) {
        addEdge(getIndex(vertex1), getIndex(vertex2), weight);
    }

    //根据顶点索引值添加边
    public void addEdge(int index1, int index2, double weight) {
        //当起点终点的索引值都存在的时候，因为是无向图，所以权重都相同
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = weight;
            adjMatrix[index2][index1] = weight;
            modCount++;
        }
    }

    @Override
    //根据顶点元素删除边
    public void removeEdge(T vertex1, T vertex2) {
        removeEdge(getIndex(vertex1), getIndex(vertex2));
    }

    //根据顶点索引值删除边
    public void removeEdge(int index1, int index2) {
        //当起点终点的索引值都存在的时候
        if (indexIsValid(index1) && indexIsValid(index2)) {
            //把边都变为正无穷大
            adjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
            adjMatrix[index2][index1] = Double.POSITIVE_INFINITY;
            modCount++;
        }
    }

    //找到两个顶点间的最短路径
    public int shortestPathLength(T startVertex, T targetVertex) {
        //根据索引值找到最短路径长度
        return shortestPathLength(getIndex(startVertex), getIndex(targetVertex));
    }

    private int shortestPathLength(int startIndex, int targetIndex) {
        int result = 0;
        //如果起始索引值或者终止索引值不存在时，返回0
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return 0;

        int index1;

        //找到起点到终点最小路径的顶点的索引值
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

        //如果没有下一个则为0，没路径
        if (it.hasNext())
            index1 = ((Integer) it.next()).intValue();
        else
            return 0;

        //有下一个则+1，作为长度
        while (it.hasNext()) {
            result++;
            it.next();
        }

        return result;
    }

    //输出最小路径中的顶点
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }
    public Iterator iteratorShortestPath(int startIndex, int targetIndex) {

        List<T> resultList = new ArrayList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return resultList.iterator();

        //得到最小路径中的顶点索引值
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);
        //把对应的顶点添加到列表里
        while (it.hasNext())
            resultList.add(vertices[((Integer) it.next()).intValue()]);
        return new GraphIterator(resultList.iterator());
    }

    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) {
        int index = startIndex;
        int[] pathLength = new int[numVertices];/*存路径的长度*/
        int[] predecessor = new int[numVertices];/*前驱顶点*/
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();/*索引值*/
        UnorderedListADT<Integer> resultList = new ArrayUnorderedList<Integer>();/*顶点元素*/

        /*如果索引值不存在或者起始终止索引值相等，返回无序列表中的顶点元素*/
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex))
            return resultList.iterator();

        /*做标记，先把标记数组全部变为false*/
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;
        /*把起始索引值加入列表*/
        traversalQueue.enqueue(Integer.valueOf(startIndex));
        /*对该索引值进行标记*/
        visited[startIndex] = true;
        /*该长度数组中该索引值的位置定位为0，前驱结点定义为-1*/
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        /*当索引值列表不为空并且起始和终止顶点元素不同时*/
        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            //index = 出队列的元素（也就是之前存入的索引值）
            index = (traversalQueue.dequeue()).intValue();

            //找出该索引值到每个顶点的边
            for (int i = 0; i < numVertices; i++) {
                //如果边存在，并且终点未标记（也就是这个边没有找到过）
                if (adjMatrix[index][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                    /*把边的终点的索引值对应的长度数组+1*/
                    pathLength[i] = pathLength[index] + 1;
                    /*前驱结点终点的索引值对应的位置存入index*/
                    predecessor[i] = index;
                    /*把新的索引值加入队列*/
                    traversalQueue.enqueue(Integer.valueOf(i));
                    /*标记该顶点*/
                    visited[i] = true;
                }
            }

        }
        if (index != targetIndex)  // no path must have been found
            return resultList.iterator();

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(Integer.valueOf(index));
        do {
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty())
            resultList.addToRear((stack.pop()));

        return new NetworkIndexIterator(resultList.iterator());
    }

    //找到最便宜的路
    public double shortestPathWeight(T vertex1, T vertex2) {
        if (shortestPathWeight(getIndex(vertex1), getIndex(vertex2)) == Double.POSITIVE_INFINITY) {
            System.out.print("No pass!  ");
            return -1;
        }
        else
            return shortestPathWeight(getIndex(vertex1), getIndex(vertex2));
    }

    public double shortestPathWeight(int start, int end) {
        Double[] dist = new Double[numVertices];//存放该顶点对所有有边顶点的权重
        boolean[] flag = new boolean[numVertices];//标记结点

        for (int i = 0; i < numVertices; i++) {
            flag[i] = false;/*让所有都变成未标记*/
            dist[i] = adjMatrix[start][i];/*初始等于该顶点到所有顶点的权重（包括不存在边的为正无穷）*/
        }
        flag[start] = true;/*把开始顶点标记*/
        int k = 0;
        for (int i = 0; i < numVertices; i++) {
            Double min = Double.POSITIVE_INFINITY;
            for (int j = 0; j < numVertices; j++) {
                //如果为标记，并且该索引值下存在权重（也就是存在边）
                if (flag[j] == false && dist[j] < min && dist[j] != -1 && dist[j] != 0) {
                    min = dist[j];/*最小值就为该权重*/
                    k = j;/*让k等于该索引值*/
                }
            }
            flag[k] = true;/*标记该索引值的顶点*/
            /*找k索引值的剩下权重是否存在，如果有找到最小的添加到min里，如果没有一直+0*/
            for (int j = 0; j < numVertices; j++) {
                if (adjMatrix[k][j] != -1&&dist[j]!= -1) {
                    double temp = (adjMatrix[k][j] == Double.POSITIVE_INFINITY ? Double.POSITIVE_INFINITY : (min + adjMatrix[k][j]));
                    //如果j未标记并且temp小于权重组中原有值，则让权重组j为索引的位置为temp
                    if (flag[j] == false && (temp < dist[j])) {
                        dist[j] = temp;
                    }
                }
            }
        }
        return dist[end];
    }

    @Override
        public Iterator iteratorBFS (T startVertex){
            return iteratorBFS(getIndex(startVertex));
        }

        public Iterator<T> iteratorBFS ( int startIndex){
            Integer x;
            QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
            UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();

            if (!indexIsValid(startIndex))
                return resultList.iterator();

            boolean[] visited = new boolean[numVertices];
            for (int i = 0; i < numVertices; i++)
                visited[i] = false;

            traversalQueue.enqueue(startIndex);
            visited[startIndex] = true;

            while (!traversalQueue.isEmpty()) {
                x = traversalQueue.dequeue();
                resultList.addToRear(vertices[x]);
                for (int i = 0; i < numVertices; i++) {
                    if (adjMatrix[x][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            }
            return new NetworkIterator(resultList.iterator());
        }

        @Override
        public Iterator iteratorDFS (T startVertex){
            return iteratorDFS(getIndex(startVertex));
        }


        public Iterator<T> iteratorDFS ( int startIndex){
            Integer x;
            boolean found;
            StackADT<Integer> traversalStack = new LinkedStack<Integer>();
            UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
            boolean[] visited = new boolean[numVertices];

            if (!indexIsValid(startIndex))
                return resultList.iterator();

            for (int i = 0; i < numVertices; i++)
                visited[i] = false;

            traversalStack.push(startIndex);
            resultList.addToRear(vertices[startIndex]);
            visited[startIndex] = true;

            while (!traversalStack.isEmpty()) {
                x = traversalStack.peek();
                found = false;

                //Find a vertex adjacent to x that has not been visited
                //     and push it on the stack
                for (int i = 0; (i < numVertices) && !found; i++) {
                    if (adjMatrix[x][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                        traversalStack.push(i);
                        resultList.addToRear(vertices[i]);
                        visited[i] = true;
                        found = true;
                    }
                }
                if (!found && !traversalStack.isEmpty())
                    traversalStack.pop();
            }
            return new NetworkIterator(resultList.iterator());
        }


        @Override
        public boolean isEmpty () {
            if (numVertices == 0)
                return true;
            else
                return false;
        }

        @Override
        public boolean isConnected () {
            boolean result = true;
            for (int i = 0; i < numVertices; i++) {
                int temp = 0;
                temp = getSizeOfIterator(this.iteratorBFS(vertices[i]));
                if (temp != numVertices) {
                    result = false;
                    break;
                }
            }
            return result;
        }

        public int getSizeOfIterator (Iterator iterator){
            int size = 0;
            while (iterator.hasNext()) {
                size++;
                iterator.next();
            }
            return size;
        }

        @Override
        public String toString () {
            {
                if (numVertices == 0)
                    return "CreatNet is empty";

                String result = new String("");

                result += "Adjacency Matrix\n";
                result += "----------------\n";
                result += "index\t";

                for (int i = 0; i < numVertices; i++) {
                    result +=  vertices[i]+" ";
                    if (i < 10)
                        result += " ";
                }
                result += "\n\n";

                for (int i = 0; i < numVertices; i++) {
                    result += vertices[i] + "\t";

                    for (int j = 0; j < numVertices; j++) {
                        if (adjMatrix[i][j] < Double.POSITIVE_INFINITY)
                            result += adjMatrix[i][j]+" ";
                        else
                            result += "===== ";
                    }
                    result += "\n";
                }


                result += "\n-------------\n";
                result += "index\tvalue\n";

                for (int i = 0; i < numVertices; i++) {
                    result += " " + i + "\t\t";
                    result += vertices[i].toString() + "\n";
                }
                result += "\n";
                return result;
            }
        }

        protected void expandCapacity () {
            T[] largerVertices = (T[]) (new Object[vertices.length * 2]);
            Double[][] largerAdjMatrix =
                    new Double[vertices.length * 2][vertices.length * 2];

            for (int i = 0; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    largerAdjMatrix[i][j] = adjMatrix[i][j];
                }
                largerVertices[i] = vertices[i];
            }

            vertices = largerVertices;
            adjMatrix = largerAdjMatrix;
        }

        @Override
        public int size () {
            return numVertices;
        }

        //判断是否有该索引值
        protected boolean indexIsValid ( int index){
            if (index < size())
                return true;
            else
                return false;
        }

        //根据顶点元素得到该索引值
        public int getIndex (T vertex){

            int index = numVertices - 1;

            for (int i = 0; i < numVertices; i++) {
                if (vertex.equals(vertices[i])) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        protected class NetworkIterator implements Iterator<T> {
            private int expectedModCount;
            private Iterator<T> iter;

            public NetworkIterator(Iterator<T> iter) {
                this.iter = iter;
                expectedModCount = modCount;
            }

            public boolean hasNext() throws ConcurrentModificationException {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }

            public T next() throws NoSuchElementException {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        protected class NetworkIndexIterator implements Iterator<Integer> {
            private int expectedModCount;
            private Iterator<Integer> iter;

            public NetworkIndexIterator(Iterator<Integer> iter) {
                this.iter = iter;
                expectedModCount = modCount;
            }

            public boolean hasNext() throws ConcurrentModificationException {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }

            public Integer next() throws NoSuchElementException {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        protected class GraphIterator implements Iterator<T> {
            private int expectedModCount;
            private Iterator<T> iter;

            public GraphIterator(Iterator<T> iter) {
                this.iter = iter;
                expectedModCount = modCount;
            }

            public boolean hasNext() throws ConcurrentModificationException {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }

            public T next() throws NoSuchElementException {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        protected class GraphIndexIterator implements Iterator<Integer> {
            private int expectedModCount;
            private Iterator<Integer> iter;

            public GraphIndexIterator(Iterator<Integer> iter) {
                this.iter = iter;
                expectedModCount = modCount;
            }

            public boolean hasNext() throws ConcurrentModificationException {
                if (!(modCount == expectedModCount))
                    throw new ConcurrentModificationException();

                return (iter.hasNext());
            }

            public Integer next() throws NoSuchElementException {
                if (hasNext())
                    return (iter.next());
                else
                    throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

    }