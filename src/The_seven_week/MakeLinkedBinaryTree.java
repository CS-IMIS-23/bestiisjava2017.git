package The_seven_week;

public class MakeLinkedBinaryTree {
    public static void main(String[] args) {
        String[] A = new String[]{"A", "B", "D", "H", "I", "E", "J", "M", "N", "C", "F", "G", "K", "L"};
        String[] B = new String[]{"H", "D", "I", "B", "E", "M", "J", "N", "A", "F", "C", "K", "G", "L"};

        LinkedBinaryTree lbt = new LinkedBinaryTree();
        lbt.buildTree(A, B);
        System.out.println(lbt);
    }
}
