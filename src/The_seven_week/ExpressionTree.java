package The_seven_week;


import The_six_week.ArrayUnorderedList;
import The_six_week.UnorderedListADT;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * ExpressionTree represents an expression tree of operators and operands.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class ExpressionTree<T> extends LinkedBinaryTree<ExpressionTreeOp> {

    public ExpressionTree() {
        super();
    }


    public ExpressionTree(ExpressionTreeOp element,
                          ExpressionTree leftSubtree, ExpressionTree rightSubtree) {
        root = new BinaryTreeNode<ExpressionTreeOp>(element, leftSubtree, rightSubtree);
    }

    //根的值
    public int evaluateTree() {
        return evaluateNode(root);
    }

    /**
     * Recursively evaluates each node of the tree.
     *
     * @param root the root of the tree to be evaluated
     * @return the integer evaluation of the tree
     */
    public int evaluateNode(BinaryTreeNode root) {
        int result, operand1, operand2;
        ExpressionTreeOp temp;

        if (root == null)
            result = 0;
        else {
            temp = (ExpressionTreeOp) root.getElement();
            //如果是操作符，左孩子为操作数1，右孩子为操作数2，如果是操作数，结果直接为操作数。
            if (temp.isOperator()) {
                operand1 = evaluateNode(root.getLeft());
                operand2 = evaluateNode(root.getRight());
                result = computeTerm(temp.getOperator(), operand1, operand2);
            } else
                result = temp.getValue();
        }

        return result;
    }

    /**
     * Evaluates a term consisting of an operator and two operands.
     *
     * @param operator the operator for the expression
     * @param operand1 the first operand for the expression
     * @param operand2 the second operand for the expression
     */
    private static int computeTerm(char operator, int operand1, int operand2) {
        int result = 0;

        if (operator == '+')
            result = operand1 + operand2;

        else if (operator == '-')
            result = operand1 - operand2;
        else if (operator == '*')
            result = operand1 * operand2;
        else
            result = operand1 / operand2;

        return result;
    }

//    public void postOrder(BinaryTreeNode<ExpressionTreeOp> root) {
//        if (root != null) {
//            postOrder(root.getLeft());
//            postOrder(root.getRight());
//            System.out.print(root.getElement() + " ");
//        }
//    }

    /**
     * Generates a structured string version of the tree by performing
     * a levelorder traversal.
     *
     * @return a string representation of this binary tree
     */
    public String printTree() {
        UnorderedListADT<BinaryTreeNode<ExpressionTreeOp>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<ExpressionTreeOp>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode<ExpressionTreeOp> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(new BinaryTreeNode(root));
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }

    public BinaryTreeNode buildTree(String str) {
        ArrayList<BinaryTreeNode> num = new ArrayList<BinaryTreeNode>();
        ArrayList<String> fuhao = new ArrayList<String>();
        StringTokenizer st = new StringTokenizer(str);
        String token;
        while (st.hasMoreTokens()) {
            token = st.nextToken();
            if (token.equals("(")) {
                String str1 = "";
                token = st.nextToken();
                while (!token.equals(")")) {
                    str1 += token + " ";
                    token = st.nextToken();
                }
                num.add(buildTree(str1));
                if (st.hasMoreTokens()) {
                    token = st.nextToken();
                }
                else
                    break;
            }
            if (!token.equals("+") && !token.equals("-") && !token.equals("*") && !token.equals("/")) {
                num.add(new BinaryTreeNode(token));
            }
            if (token.equals("+") || token.equals("-")) {

                BinaryTreeNode<String> tempNode = new BinaryTreeNode<>(token);
                token = st.nextToken();
                if (!token.equals("(")) {
                    fuhao.add(tempNode.element);
                    num.add(new BinaryTreeNode(token));
                }
                else {
                    fuhao.add(tempNode.element);
                    String temp = st.nextToken();
//                    tempNode.setLeft(num.remove(num.size() - 1));
                    String s = "";
                    while (!temp.equals(")")) {
                        s += temp + " ";
                        temp = st.nextToken();
                    }
                    num.add(buildTree(s));
                }
            }
            if (token.equals("*") || token.equals("/")) {
                BinaryTreeNode<String> tempNode = new BinaryTreeNode<>(token);
                token = st.nextToken();
                if (!token.equals("(")) {
                    tempNode.setLeft(num.remove(num.size() - 1));
                    tempNode.setRight(new BinaryTreeNode<String>(token));
                    num.add(tempNode);
                }
                else {
                    String temp = st.nextToken();
                    tempNode.setLeft(num.remove(num.size() - 1));
                    String s = "";
                    while (!temp.equals(")")) {
                        s += temp + " ";
                        temp = st.nextToken();
                    }
                    tempNode.setRight(buildTree(s));
                    num.add(tempNode);
                }
            }
        }
        int i = fuhao.size();
        while (i > 0) {
            BinaryTreeNode<T> root = new BinaryTreeNode(fuhao.remove(fuhao.size() - 1));
            root.setRight(num.remove(num.size() - 1));
            root.setLeft(num.remove(num.size() - 1));
            num.add(root);
            i--;
        }
        return num.get(0);
    }
}
//                  else
//                numList.add(new LinkedBinaryTree(token));

//                if (!token.equals("(")) {
//                    LinkedBinaryTree right = new LinkedBinaryTree(token);
//                    LinkedBinaryTree node = new LinkedBinaryTree(A, left, right);
//                    numList.add(node);
//                } else {
//                    String str1 = "";
//                    while (true) {
//                        token = st.nextToken();
//                        if (!token.equals(")"))
//                            str1 += token + " ";
//                        else
//                            break;
//                    }
//                    LinkedBinaryTree temp2 = new LinkedBinaryTree();
//                    temp2.root = buildTree(str1);
//                    LinkedBinaryTree node1 = new LinkedBinaryTree(A, left, temp2);
//                    numList.add(node1);
//                }
//            }


