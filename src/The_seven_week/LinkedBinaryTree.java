package The_seven_week;


import The_six_week.UnorderedListADT;
import The_six_week.ArrayUnorderedList;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;


public class LinkedBinaryTree<T> implements BinaryTreeADT<T> {
    protected BinaryTreeNode<T> root;
    protected int modCount;
    protected String res = "";


    public LinkedBinaryTree() {
        root = null;
    }


    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }


    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T find(T targetElement) throws ElementNotFoundException {
        BinaryTreeNode<T> current = findAgain(targetElement, root);
        if (current == null)
            throw new ElementNotFoundException("binary tree");

        return (current.element);
    }

    //先判断根root是否为空，如果为空则null，如果元素相等则返回root结点，
    // 否则开始进行递归，先判断左孩子是否为空，为空则null，如果元素相等则返回左孩子结点，不相等再进行递归
    // 直到为null时，temp也为null，开始判断右孩子，与上同理，进行递归，最后得到结果。
    private BinaryTreeNode<T> findAgain(T targetElement, BinaryTreeNode<T> next) {
        if (next == null)
            return null;
        if (next.element.equals(targetElement))
            return next;
        BinaryTreeNode<T> temp = findAgain(targetElement, next.left);

        if (temp == null)
            temp = findAgain(targetElement, next.right);

        return temp;
    }


    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        return root;
    }

    public LinkedBinaryTree<T> removeAllElements(LinkedBinaryTree<T> next) {
        if (next.root == null)
            throw new ElementNotFoundException("binary tree");
        else
            next.root = null;
        return next;
    }

    //移出所选结点的右孩子
    public BinaryTreeNode<T> removeRightSubtree(BinaryTreeNode<T> targetElement, BinaryTreeNode<T> next) {
        find(targetElement.element);
        targetElement.setRight(null);
        return next;
    }

    public boolean isWhat(LinkedBinaryTree<T> next) {
        if (next.root != root) {
            if (next.getLeft() == null && next.getRight() == null)
                return true;
            else
                return false;
        } else
            throw new ElementNotFoundException("is Root");

    }

    @Override
    public T getRootElement() {
        return root.element;
    }

    @Override
    public boolean isEmpty() {
        return (root == null);
    }

    @Override
    public int size() {
        return num(root);
    }

    private int num(BinaryTreeNode<T> next) {
        if (next == null) {
            return 0;
        } else {
            return 1 + num(next.left) + num(next.right);
        }
    }

    @Override
    public boolean contains(T targetElement) {
        BinaryTreeNode<T> current = findAgain(targetElement, root);
        if (current == null)
            return false;
        else
            return true;
    }


    public BinaryTreeNode<T> getLeft() {
        return root.left;
    }

    public BinaryTreeNode<T> getRight() {
        return root.right;
    }

    public BinaryTreeNode<T> setLeft(BinaryTreeNode node) {
        root.left = node;
        return root.left;
    }

    public BinaryTreeNode<T> setRight(BinaryTreeNode node) {
        root.right = node;
        return root.right;
    }


    //前序遍历
    public void preOrder(BinaryTreeNode<T> root) {
        if (root != null) {
            System.out.print(root.element + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    //中序遍历
    public void inOrder(BinaryTreeNode<T> root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.element + " ");
            inOrder(root.right);

        }
    }

    public Iterator<T> iteratorPostorder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }

    //后序遍历
    public void postOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
        if (node != null) {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    public String getRes() {
        return res;
    }

    //非递归层序遍历
    public void levekOrder() {
        if (root != null) {
            LinkedList<BinaryTreeNode> queue = new LinkedList<>();
            BinaryTreeNode<T> p;
            queue.push(root);

            while (!queue.isEmpty()) {
                p = queue.removeFirst();
                System.out.println(p.element);
                if (p.left != null)
                    queue.addLast(p.left);
                if (p.right != null)
                    queue.addLast(p.right);
            }
        } else
            System.out.println("null");
    }

    //递归层序遍历
    public void getlevelOrder(BinaryTreeNode<T> Node) {
        if (Node == null) {
            return;
        }
        int depth = Height(root);

        for (int i = 1; i <= depth; i++) {
            levelOrder(Node, i);
        }
    }

    private void levelOrder(BinaryTreeNode<T> Node, int level) {
        if (Node == null || level < 1) {
            return;
        }
        if (level == 1) {
            System.out.println(Node.element + "  ");
            return;
        }
        // 左子树
        levelOrder(Node.left, level - 1);

        // 右子树
        levelOrder(Node.right, level - 1);
    }

    public int getHeight() {
        return Height(root) - 1;
    }

    private int Height(BinaryTreeNode<T> node) {
        if (node == null) {
            return 0;
        }
        int ldep = Height(node.left);
        int rdep = Height(node.right);
        if (ldep > rdep) {
            return ldep + 1;
        } else {
            return rdep + 1;
        }
    }

    public String toString() {
        UnorderedListADT<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<T>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode<T> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                previousLevel = currentLevel;
                result = result + "\n" + "\n";

                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++) {
                    result = result + " ";
                }

            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }


            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else

            {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }

    public int CountLeaf(BinaryTreeNode node) {
        if (node != null) {
            if (node.left == null && node.right == null) {
                return 1;
            }
            return CountLeaf(node.left) + CountLeaf(node.right);
        }
        return 0;
    }

    public int Deep(BinaryTreeNode<T> node) {
        if (node == null) {
            return 0;
        }
        int ldep = Height(node.left);
        int rdep = Height(node.right);
        if (ldep > rdep) {
            return ldep + 1;
        } else {
            return rdep + 1;
        }
    }

    public void buildTree(T[] preorder, T[] inorder) {
        BinaryTreeNode temp = makeTree(preorder, 0, preorder.length, inorder, 0, inorder.length);
        root = temp;

    }


    public BinaryTreeNode<T> makeTree(T[] preorder, int start, int len, T[] inorder, int start1, int len1) {
        if (len < 1) {
            return null;
        }
        BinaryTreeNode root;
        //先序的第一个元素为根
        root = new BinaryTreeNode(preorder[start]);
        int n = 0;
        for (int temp = 0; temp < len1; temp++) {
            //找到在中序中的根的位置并记录
            if (inorder[start1 + temp] == root.element) {
                n = temp;
                break;
            }
        }
        //左子树的建立，先序的左子树开始是下一位，结束是中序的开始到记录的位置，中序的左子树开始不变，结束的记录的位置
        root.setLeft(makeTree(preorder, start + 1, n, inorder, start1, n));
        //右子树的建立，先序的右子树开始是左子树的根位置加上中序的记录的位置，结束是最后的索引值减去记录的位置。
        root.setRight(makeTree(preorder, start + n + 1, len - n - 1, inorder, start1 + n + 1, len1 - n - 1));
        return root;
    }


//    public Iterator<T> iterator() {
//        return iteratorInOrder();
//    }

    /**
     * Performs an inorder traversal on this binary tree by calling an
     * overloaded, recursive inorder method that starts with
     * the root.
     *
     * @return an in order iterator over this binary tree
     */
//    public Iterator<T> iteratorInOrder() {
//        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
//        inOrder(root, tempList);
//
//        return new TreeIterator(tempList.iterator());
//    }
//
//    /**
//     * Performs a recursive inorder traversal.
//     *
//     * @param node     the node to be used as the root for this traversal
//     * @param tempList the temporary list for use in this traversal
//     */
//    protected void inOrder(BinaryTreeNode<T> node,
//                           ArrayUnorderedList<T> tempList) {
//        if (node != null) {
//            inOrder(node.getLeft(), tempList);
//            tempList.addToRear(node.getElement());
//            inOrder(node.getRight(), tempList);
//        }
//    }
//
//    /**
//     * Performs an preorder traversal on this binary tree by calling
//     * an overloaded, recursive preorder method that starts with
//     * the root.
//     *
//     * @return a pre order iterator over this tree
//     */
//    public Iterator<T> iteratorPreOrder() {
//        // To be completed as a Programming Project
//    }
//
//    /**
//     * Performs a recursive preorder traversal.
//     *
//     * @param node     the node to be used as the root for this traversal
//     * @param tempList the temporary list for use in this traversal
//     */
//    protected void preOrder(BinaryTreeNode<T> node,
//                            ArrayUnorderedList<T> tempList) {
//        // To be completed as a Programming Project
//    }
//
//    /**
//     * Performs an postorder traversal on this binary tree by calling
//     * an overloaded, recursive postorder method that starts
//     * with the root.
//     *
//     * @return a post order iterator over this tree
//     */
//    public Iterator<T> iteratorPostOrder() {
//        // To be completed as a Programming Project
//    }
//
//    /**
//     * Performs a recursive postorder traversal.
//     *
//     * @param node     the node to be used as the root for this traversal
//     * @param tempList the temporary list for use in this traversal
//     */
//    protected void postOrder(BinaryTreeNode<T> node,
//                             ArrayUnorderedList<T> tempList) {
//        // To be completed as a Programming Project
//    }
//
//    /**
//     * Performs a levelorder traversal on this binary tree, using a
//     * templist.
//     *
//     * @return a levelorder iterator over this binary tree
//     */
//    public Iterator<T> iteratorLevelOrder() {
//        ArrayUnorderedList<BinaryTreeNode<T>> nodes =
//                new ArrayUnorderedList<BinaryTreeNode<T>>();
//        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
//        BinaryTreeNode<T> current;
//
//        nodes.addToRear(root);
//
//        while (!nodes.isEmpty()) {
//            current = nodes.removeFirst();
//
//            if (current != null) {
//                tempList.addToRear(current.getElement());
//                if (current.getLeft() != null)
//                    nodes.addToRear(current.getLeft());
//                if (current.getRight() != null)
//                    nodes.addToRear(current.getRight());
//            } else
//                tempList.addToRear(null);
//        }
//
//        return new TreeIterator(tempList.iterator());
//    }
//
//    /**
//     * Inner class to represent an iterator over the elements of this tree
//     */
    public class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a tree traversal
         */
        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}



