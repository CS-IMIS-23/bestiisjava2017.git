package The_seven_week;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree a = new LinkedBinaryTree(1);
        LinkedBinaryTree b = new LinkedBinaryTree(3);
        LinkedBinaryTree c = new LinkedBinaryTree(8, a, b);
        LinkedBinaryTree d = new LinkedBinaryTree(10);
        LinkedBinaryTree e = new LinkedBinaryTree(24);
        LinkedBinaryTree f = new LinkedBinaryTree(5, d, e);
        LinkedBinaryTree g = new LinkedBinaryTree(0, c, f);
        System.out.println("前序：");
        g.preOrder(g.root);
        System.out.println();
        System.out.println("后序：");
        g.postOrder(g.root);
        System.out.println();
        System.out.println("toString："+g.toString());
        System.out.println("contain 8?:"+g.contains(8));
        System.out.println("getRight:"+g.getRight().element);
    }
}
