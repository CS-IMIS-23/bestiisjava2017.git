package The_seven_week;


public class AVLBinarySearchTree<T extends Comparable<T>> {
    AVLTreeNode<T> root;
    int num = 0;

    /**
     * Creates an empty binary search tree.
     */
    public AVLBinarySearchTree() {
        root = null;
    }

    public AVLBinarySearchTree(T element) {
        root = new AVLTreeNode<T>(element);
    }

    /*
     * 获取树的高度
     */
    public int height() {
        return height(root);
    }

    private int height(AVLTreeNode<T> node) {
        if (node == null) {
            return 0;
        }
        int ldep = height(node.getLeft());
        int rdep = height(node.getRight());
        if (ldep > rdep) {
            return ldep + 1;
        } else {
            return rdep + 1;
        }
    }

    //比较两个值的大小,用于找出子树的高度。
    private int max(int a, int b) {
        if (a > b)
            return a;
        else
            return b;
    }

    //右旋
    private AVLTreeNode<T> leftLeftRotation(AVLTreeNode<T> node) {
        AVLTreeNode<T> current;

        current = node.getLeft();//新的根结点=原根结点的左孩子
        node.setLeft(current.getRight());//原根结点的做孩子是新根结点的右孩子
        current.setRight(node);//新的根结点的右孩子为原根结点


        node.height = max(height(node.getLeft()), height(node.getRight())) + 1;//得出原根的现高度
        current.height = max(height(current.getLeft()), node.height) + 1;//得出新根的现高度

        return current;
    }

    //左旋
    private AVLTreeNode<T> rightRightRotation(AVLTreeNode<T> node) {
        AVLTreeNode<T> current;

        current = node.getRight();//新根是原根的右结点
        node.setRight(current.getLeft());//原根的右结点是新根的左孩子
        current.setLeft(node);//新根的左结点时原根

        node.height = max(height(node.getLeft()), height(node.getRight())) + 1;//得出原根的现高度
        current.height = max(height(current.getRight()), node.height) + 1;//得出新根的现高度

        return current;
    }

    //左右旋
    private AVLTreeNode<T> leftRightRotation(AVLTreeNode<T> node) {
        AVLTreeNode<T> current;
        current = rightRightRotation(node.getLeft());
        node.setLeft(current);

        return leftLeftRotation(node);
    }

    //右左旋
    private AVLTreeNode<T> rightLeftRotation(AVLTreeNode<T> node) {
        AVLTreeNode<T> current;
        current = leftLeftRotation(node.getRight());
        node.setRight(current);

        return rightRightRotation(node);
    }

    private AVLTreeNode<T> insert(AVLTreeNode<T> tree, T key) {
        AVLTreeNode current;
        if (tree == null) {
            // 新建结点
            tree = new AVLTreeNode<T>(key, null, null);
            if (tree == null) {
                System.out.println("ERROR: create avltree node failed!");
                return null;
            }
        } else {
            //找到插入的元素应该在左子树还是右子树
            int res = key.compareTo(tree.element);
            //比根结点小
            if (res < 0) {    // 插入到"左子树"的情况
                current = insert(tree.getLeft(), key);
                tree.setLeft(current);
                // 插入结点后，若失去平衡，则进行相应的调节。
                if (height(tree.getLeft()) - height(tree.getRight()) == 2) {
                    if (key.compareTo(tree.getLeft().element) < 0) /*左结点的左子树上做了插入元素的操作，进行右旋*/
                        tree = leftLeftRotation(tree);
                    else                                            /*一次旋转不够，进行右左旋*/
                        tree = leftRightRotation(tree);
                }
            } else if (res > 0) {    // 插入到"右子树"的情况
                current = insert(tree.getRight(), key);
                tree.setRight(current);
                // 插入结点后，若失去平衡，则进行相应的调节。
                if (height(tree.getRight()) - height(tree.getLeft()) == 2) {
                    if (key.compareTo(tree.getRight().element) > 0)/*右结点的右子树上做了插入元素的操作，进行左旋*/
                        tree = rightRightRotation(tree);
                    else                                            /*一次旋转不够，进行左右旋*/
                        tree = rightLeftRotation(tree);
                }
            } else {    //相等结点
                System.out.println("相等结点！");
            }
        }

        tree.height = max(height(tree.getLeft()), height(tree.getRight())) + 1;//得到现在的新树的高度

        return tree;
    }

    public void insert(T key) {
        root = insert(root, key);
    }


//    public boolean isEmpty(){
//        if (num == 0)
//            return false;
//        return true;
//    }
//    public T findMin() throws EmptyCollectionException {
//        T result = null;
//
//        if (isEmpty())
//            throw new EmptyCollectionException("LinkedBinarySearchTree");
//        else {
//            if (root.getLeft() == null) {
//                result = root.element;
//                root = root.getRight();
//            } else {
//                AVLTreeNode<T> current = root.getLeft();
//                while (current.getLeft() != null) {
//                    current = current.getLeft();
//                }
//                result = current.element;
//            }
//
//        }
//        return result;
//    }
//    public AVLTreeNode findMax() throws EmptyCollectionException {
//        AVLTreeNode result = null;
//
//        if (isEmpty())
//            throw new EmptyCollectionException("LinkedBinarySearchTree");
//        else {
//            if (root.getRight() == null) {
//                result = root.element;
//                root = root.getRight();
//            } else {
//                AVLTreeNode<T> parent = root;
//                AVLTreeNode<T> current = root.getRight();
//                while (current.getRight() != null) {
//                   current = current.getRight();
//                }
//                result = current.element;
//            }
//        }
//        return result;
//    }

    private AVLTreeNode<T> search(AVLTreeNode<T> tree, T key) {
        if (tree == null)
            return tree;

        int res = key.compareTo(tree.element);
        if (res < 0)
            return search(tree.getLeft(), key);
        else if (res > 0)
            return search(tree.getRight(), key);
        else
            return tree;
    }

    public AVLTreeNode<T> search(T key) {
        return search(root, key);
    }

//    //找到最小的结点
//    private AVLTreeNode<T> minimum(AVLTreeNode<T> tree) {
//        if (tree == null)
//            return null;
//
//        while (tree.getLeft() != null)
//            tree = tree.getLeft();
//        return tree;
//    }
//
//    public T minimum() {
//        AVLTreeNode<T> p = minimum(root);
//        if (p != null)
//            return p.element;
//
//        return null;
//    }
//
//    //找到最大的结点
//    private AVLTreeNode<T> maximum(AVLTreeNode<T> tree) {
//        if (tree == null)
//            return null;
//
//        while (tree.getRight() != null)
//            tree = tree.getRight();
//        return tree;
//    }
//
//    public T maximum() {
//        AVLTreeNode<T> p = maximum(root);
//        if (p != null)
//            return p.element;
//
//        return null;
//    }

    public AVLTreeNode<T> findMin(AVLTreeNode<T> root) throws EmptyCollectionException {
        AVLTreeNode<T> result = root;

        if (root.getLeft() == null) {
            result = root;
        } else {
            while (result.getLeft() != null) {
                result = result.getLeft();
            }
        }
        return result;
    }
    public AVLTreeNode<T> findMax(AVLTreeNode<T> root) throws EmptyCollectionException {
        AVLTreeNode<T> result = root;

        if (root.getRight() == null) {
            result = root;
        } else {
            while (result.getRight() != null) {
                result = result.getLeft();
            }
        }
        return result;
    }


    private AVLTreeNode<T> remove(AVLTreeNode<T> tree, AVLTreeNode<T> ele) {

        // 根为空 或者 没有要删除的节点，直接返回null。
        AVLTreeNode<T> current;
        if (tree == null || ele == null)
            return null;

        int res = ele.element.compareTo(tree.element);
        if (res < 0) {        // 删除的结点在左子树中
            current = remove(tree.getLeft(), ele);//移出后的结果
            tree.setLeft(current);

            // 删除后，失去平衡，则进行相应的调节。
            if (height(tree.getRight()) - height(tree.getLeft()) == 2) {
                AVLTreeNode<T> r = tree.getRight();
                if (height(r.getLeft()) > height(r.getRight()))
                    tree = rightLeftRotation(tree);
                else
                    tree = rightRightRotation(tree);
            }
        } else if (res > 0) {    // 删除的结点在右子树中
            current = remove(tree.getRight(), ele);
            tree.setRight(current);

            // 删除后，失去平衡，则进行相应的调节。
            if (height(tree.getLeft()) - height(tree.getRight()) == 2) {
                AVLTreeNode<T> l = tree.getLeft();
                if (height(l.getRight()) > height(l.getLeft()))
                    tree = leftRightRotation(tree);
                else
                    tree = leftLeftRotation(tree);
            }
        } else {
            // 删除的是根结点
            // tree的左右孩子都非空
            if ((tree.getLeft() != null) && (tree.getRight() != null)) {
                if (height(tree.getLeft()) > height(tree.getRight())) {
                    // 如果tree的左子树比右子树高；则找出tree的左子树中的最大节点.将该最大节点的值赋值给tree。删除该最大节点。
                    AVLTreeNode<T> max = findMax(tree.getLeft());
                    tree.element = max.element;
                    tree.setLeft(remove(tree.getLeft(), max));
                } else {
                    //左子树和右子树相等，或右子树比左子树高，找出tree的右子树中的最小节点，将该最小节点的值赋值给tree。删除该最小节点。
                    AVLTreeNode<T> min = findMin(tree.getRight());
                    tree.element = min.element;
                    tree.setRight(remove(tree.getRight(), min));
                }
            }
            else {
                if (tree.getLeft() != null)
                    tree = tree.getLeft();
                else
                    tree = tree.getRight();
            }
        }
        return tree;
    }

    public void remove(T key) {
        AVLTreeNode<T> z;

        if ((z = search(key)) != null)
            root = remove(root, z);
    }

    public String printTree() {
        UnorderedListADT<AVLTreeNode<T>> nodes =
                new ArrayUnorderedList<AVLTreeNode<T>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        AVLTreeNode<T> current;
        String result = "";
        int printDepth = this.height();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }
}