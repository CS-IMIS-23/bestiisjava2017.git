package The_seven_week;

public class AVLTreeNode<T extends Comparable<T>> {
    private AVLTreeNode<T> root;    // 根结点
    private AVLTreeNode<T> left,right;    // 左孩子
    T element;                // 关键字(键值)
    int height;         // 高度

    public AVLTreeNode(T element) {
        this.element = element;
        this.left = null;
        this.right = null;
        this.height = 0;
    }
    public AVLTreeNode(T element, AVLTreeNode<T> left, AVLTreeNode<T> right) {
        this.element = element;
        this.left = left;
        this.right = right;
        this.height = 0;
    }

    public int numChildren() {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    public T getElement() {
        return element;
    }


    public AVLTreeNode<T> getRight() {
        return right;
    }

    public void setRight(AVLTreeNode<T> node) {
        right = node;
    }

    public AVLTreeNode<T> getLeft() {
        return left;
    }

    public void setLeft(AVLTreeNode<T> node) {
        left = node;
    }
}