package The_seven_week;

public class LinkedBinarySearchTreeTest {
    public static void main(String[] args) {
        LinkedBinarySearchTree a = new LinkedBinarySearchTree(6);
        a.addElement(10);
        a.addElement(0);
        a.addElement(3);
        a.addElement(5);
        a.addElement(8);
        a.addElement(20);
        System.out.println("树状：");
        System.out.println(a.toString());
        System.out.println("max："+a.findMax());
        System.out.println("min："+a.findMin());
        a.removeMax();
        System.out.println("remove max:"+a.toString());
        a.removeMin();
        System.out.println("remove min:"+a.toString());

    }
}
