package The_seven_week;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Demonstrates the use of an expression tree to evaluate postfix expressions.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class PostfixTester {
    /**
     * Reads and evaluates multiple postfix expressions.
     */
    public static void main(String[] args) {
        String expression, again;
        String res = "";
        int result;

        Scanner in = new Scanner(System.in);
        PostfixEvaluator evaluator = new PostfixEvaluator();
        expression = in.nextLine();
        ExpressionTree et = new ExpressionTree();
        LinkedBinaryTree tree = new LinkedBinaryTree();
        tree.root = et.buildTree(expression);
        Iterator it = tree.iteratorPostorder();
        while (it.hasNext())
            res += (it.next())+" ";
        System.out.println("houzhui:" + res);
        System.out.println(tree.toString());
        result = evaluator.evaluate(res);
        System.out.println("That expression equals " + result);



//        do
//        {
//            PostfixEvaluator evaluator = new PostfixEvaluator();
////            System.out.println("Enter a valid post-fix expression one token " +
////                               "at a time with a space between each token (e.g. 5 4 + 3 2 1 - + *)");
////            System.out.println("Each token must be an integer or an operator (+,-,*,/)");
//            expression = in.nextLine();
//            ExpressionTree et= new ExpressionTree();
//
//            BinaryTreeNode node= et.buildTree(expression);
//
//            LinkedBinaryTree tree = new LinkedBinaryTree();
//            tree.root = node;
//            tree.postOrder(node);
//            System.out.println(tree.getRes());
////            result = evaluator.evaluate(tree.getRes());
//            System.out.println();
////            System.out.println("That expression equals " + result);
//
//            System.out.println("The Expression Tree for that expression is: ");
//            System.out.println(evaluator.getTree());
//
//            System.out.print("Evaluate another expression [Y/N]? ");
//            again = in.nextLine();
//            System.out.println();
//        }
//        while (again.equalsIgnoreCase("y"));
    }
}
