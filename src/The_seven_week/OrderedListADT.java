package The_seven_week;

import The_four_week.ListADT;

public interface OrderedListADT<T> extends ListADT<T>
{

    public void add(T element);
}