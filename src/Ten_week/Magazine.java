package Ten_week;//********************************************************************
//  Magazine.java       Author: Lewis/Loftus
//
//  Represents a single magazine.
//********************************************************************

public class Magazine implements Comparable {
    private String title;

    //-----------------------------------------------------------------
    //  Sets up the new magazine with its title.
    //-----------------------------------------------------------------
    public Magazine(String newTitle) {
        title = newTitle;
    }

    //-----------------------------------------------------------------
    //  Returns this magazine as a string.
    //-----------------------------------------------------------------
    public String toString() {
        return title;
    }

    public int compareTo(Object o) {
        int result;
        result = title.compareTo(((Magazine)o).title);
        return result;
    }
}

