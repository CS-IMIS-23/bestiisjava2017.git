package Ten_week;//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

import eight_week.Sorting;

import java.util.ArrayList;

public class MagazineList {
    private MagazineNode list;
    ArrayList<Magazine> ma = new ArrayList<Magazine>();

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag) {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;
        int index = 0;
        if (list == null) {
            list = node;
        } else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }


    //在index的位置插入新节点newMagazine
    public void insert(int index, Magazine newMagazine) {
        MagazineNode node = new MagazineNode(newMagazine);
        MagazineNode h1, h2;
        if (index == 0) {
            node.next = list;
            list = node;
        }
        if (index < size()) {
            int i = 1;
            h1 = list;
            h2 = list;
            while (h2.next != null) {
                h2 = h2.next;
                if (i == index) {
                    node.next = h2;
                    h1.next = node;
                } else {
                    h1 = h2;
                    i++;
                }
            }
        }
        if (index >= size()) {
            add(newMagazine);
        }
    }

    public int size() {
        MagazineNode head = list;
        int res = 0;
        if (head != null) {
            res++;
        }
        while (head.next != null) {
            head = head.next;
            res++;
        }
        return res;
    }

    //删除节点delNode
    public void delete(Magazine delNode) {
        MagazineNode node = new MagazineNode(delNode);
        MagazineNode h1, h2;
        if (node.magazine.toString().equals(list.magazine.toString())) {
            list = list.next;
        } else {
            h1 = list;
            h2 = list;
            while (h2.next != null) {
                h2 = h2.next;
                if (h2.magazine.toString().equals(node.magazine.toString())) {
                    if (h2.next != null)
                        h1.next = h2.next;
                    else
                        h1.next = null;
                } else
                    h1 = h1.next;
            }
            if (h2.magazine.toString().equals(node.magazine.toString()) && (h2.next == null)) {
                h1.next = null;
            }
        }
    }


    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        MagazineNode current = list;

        while (current != null) {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }
    public void paixu(){
        MagazineNode h = list;
        while (h.next != null){
            ma.add(h.magazine);
            h = h.next;
        }
        ma.add(h.magazine);

        Magazine[] se = new Magazine[ma.size()];
        for (int i = 0;i < ma.size();i++)
            se[i] = ma.get(i);
        selectionSort(se);
        for (int j = 0;j <ma.size();j++)
        System.out.println(se[j]);
    }

    public void selectionSort(Comparable[] list) {
        for (int index = 1; index < list.length; index++) {
            Comparable key = list[index];
            int position = index;

            while (position > 0 && key.compareTo(list[position - 1]) < 0) {
                list[position] = list[position - 1];
                position--;
            }

            list[position] = key;
        }
    }

    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag) {
            magazine = mag;
            next = null;
        }
    }
}
