import java.io.*;
import javax.crypto.*;
public class AES{
    public static void main(String args[])
            throws Exception{
        KeyGenerator kg=KeyGenerator.getInstance("AES");
        kg.init(256);
        SecretKey k=kg.generateKey( );
        FileOutputStream  f=new FileOutputStream("key.dat");
        ObjectOutputStream b=new  ObjectOutputStream(f);
        b.writeObject(k);
    }
}