import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by besti on 2018/6/9.172.16.
 */
public class SocketServer {
    public static void main(String[] args) throws Exception {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(8080);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info, info1, info2 = null;
        List<String> A = new ArrayList<String>();
        double result;

        FileOutputStream f1 = new FileOutputStream("SEnc.dat");

        if (!((info = bufferedReader.readLine()) == null)) {
            System.out.println("miwen:" + info);
            StringTokenizer st = new StringTokenizer(info);
            while (st.hasMoreTokens()) {
                A.add(st.nextToken());
            }
        }
        if (!((info1 = bufferedReader.readLine()) == null)) {
            System.out.println("miwenMD5:" + info1);
        }
        if (!((info2 = bufferedReader.readLine()) == null)) {
            System.out.println("mingwenMD5:" + info2);
        }
        //密文
        byte[] ctext = new byte[A.size()];
        for (int d = 0; d < A.size(); d++) {

            ctext[d] = Byte.valueOf(A.get(d));
        }
//        f1.write(ctext);

//        // 获取密文
//        FileInputStream f = new FileInputStream("SEnc.dat");
//        int num = f.available();
//        byte[] ctext = new byte[num];
//        f.read(ctext);

        // 获取密钥
        KeyAgree ke = new KeyAgree();
        byte[] de = ke.key();
        SecretKeySpec k = new SecretKeySpec(de, 0, 24, "AES");
//        FileInputStream f3 = new FileInputStream("keyDH.dat");
//        int num2 = f3.available();
//        byte[] keykb = new byte[num2];
//        f3.read(keykb);

        // 解密
        Cipher cp = Cipher.getInstance("AES");
        cp.init(Cipher.DECRYPT_MODE, k);
        byte[] ptext = cp.doFinal(ctext);

        // 显示明文
        String p = new String(ptext, "UTF8");
        System.out.println("poxtfix:" + p);
        DigestPass digestPass = new DigestPass();
        String d = digestPass.MD5(p);
        if (d.equals(info2)) {
            //计算
            MyDC dc = new MyDC();
            List<String> str = new ArrayList<>();
            StringTokenizer sb = new StringTokenizer(p);
            while (sb.hasMoreTokens()) {
                str.add(sb.nextToken());
            }
            result = dc.doCal(str);
            printWriter.flush();
            System.out.println("result:" + result);

            //给客户一个响应
            printWriter.write(result + "");
            printWriter.flush();
        }
        else {
            //计算
            MyDC dc = new MyDC();
            List<String> str = new ArrayList<>();
            StringTokenizer sb = new StringTokenizer(p);
            while (sb.hasMoreTokens()) {
                str.add(sb.nextToken());
            }
            result = dc.doCal(str);
            printWriter.flush();
            System.out.println("false.  result:" + result);

            //给客户一个响应
            printWriter.write("false. result:"+ result + "");
            printWriter.flush();
            printWriter.flush();
        }

            //5.关闭资源
            printWriter.close();
            outputStream.close();
            bufferedReader.close();
            inputStream.close();
            socket.close();
            serverSocket.close();
        }
    }

