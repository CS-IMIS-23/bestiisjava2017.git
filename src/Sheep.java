public class Sheep extends Animal {
    private String a, b, c;

    public Sheep(String name, int id) {
        super(name, id);
    }
    @Override
    public void eat() {
        a = "羊吃草。";

    }
    @Override
    public void sleep() {
        b = "羊站着睡。";
    }
    @Override
    public void introduction() {
        c = "羊是羊亚科的统称，哺乳纲、偶蹄目、牛科、羊亚科，是人类的家畜之一。";
    }
    public String getEat() {
        return a;
    }
    public String getSleep() {
        return b;
    }
    public String getIntroduction() {
        return c;
    }
}