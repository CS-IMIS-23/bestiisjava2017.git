package eight_week;

import java.text.NumberFormat;

public class DVD implements Comparable{
    private String title,director;
    private int year;
    private double cost;
    private boolean bluray;

    //------------------------------------------------------------------------------------------------------------------
    //Creates a new DVD with the specified information.
    //------------------------------------------------------------------------------------------------------------------
    public DVD(String title, String director, int year, double cost, boolean bluray)
    {
        this.title = title;
        this.director = director;
        this.year = year;
        this.cost = cost;
        this.bluray = bluray;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Returns a string description of this DVD.
    //------------------------------------------------------------------------------------------------------------------
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String description;

        description = fmt.format(cost) + "\t" + year + "\t";
        description += title + "\t" + director;
        if (bluray)
            description += "\t" + "Blu-ray";

        return description;
    }
    public String getTitle(){
        return title;
    }

    @Override
    public int compareTo(Object o) {
        int result;

        String otherFirst = ((DVD)o).getTitle();
        result = otherFirst.compareTo(getTitle() );

        return result;
    }
}
