package seven_week;

public class Book3 {
    protected int pages;
    protected String word;
    protected String bookname;
    //设置书的页数
    public void setPages(int numPages)
    {
        pages = numPages;
    }
    //返回书的页数
    public int getPages() {
        return pages;
    }
    //设置书的关键词
    public void setWord(String bookword){
        word = bookword;
    }
    //返回书的关键词
    public String getWord(){
        return word;
    }
    //设置书的名字
    public void setBookname(String name){
        bookname = name;
    }
    //返回书的关键词
    public String getBookname(){
        return bookname;
    }

}
