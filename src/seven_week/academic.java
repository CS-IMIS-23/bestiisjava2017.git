package seven_week;

public class academic extends Book3 {
    private String prefosserName;

    //设置专家名字
    public void setacademic(String name) {
        prefosserName = name;
    }

    //返回专家名字
    public String getacademic() {
        return prefosserName;
    }

    //学术刊物概述
    public String toString() {
        String result;
        result = "书名:"+ bookname+"\t\t\t\t"+"页数:"+pages+"\t\t\t\t"+"关键词:"+word+"\t\t\t\t"+"专家名字:"+prefosserName;
        return result;
    }
}
