package seven_week;

public class Book3Test {
    public static void main(String[] args) {
        magazine a = new magazine();
        novel b = new novel();
        academic c = new academic();

        //杂志
        a.setBookname("意林");
        a.setPages(180);
        a.setWord("杂志");
        a.setHeadLine("×××");
        System.out.println(a);

        //小说
        b.setBookname("凤囚凰");
        b.setPages(240);
        b.setWord("古代");
        b.setIntroduction("穿越后的狗血爱情故事。");
        b.setAuthor("天衣有风");
        System.out.println(b);

        //学术刊物
        c.setBookname("aaa");
        c.setPages(240);
        c.setWord("Java");
        c.setacademic("bbb");
        System.out.println(c);
    }
}
