package seven_week;

public class novel extends Book3 {
    private String introduction;
    private String author;

    //设置小说简介
    public void setIntroduction(String a) {
        introduction = a;
    }

    //返回小说简介
    public String getIntroduction() {
        return introduction;
    }

    //设置小说的作家
    public void setAuthor(String name) {
        author = name;
    }

    //返回小说的作家
    public String getAuthor() {
        return author;
    }

    //小说概述
    public String toString() {
        String result;
        result = "书名:" + bookname + "\t\t\t\t" + "页数:" + pages + "\t\t\t\t" + "关键词:" + word + "\t\t\t\t" + "小说简介:" + introduction + "\t\t\t\t" + "小说作家:" + author;
        return result;
    }
}
