package seven_week;
import java.util.Scanner;
public class MonetaryCoinTest {
    public static void main(String[] args) {
        String a;
        int sum = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("是否开始：");
        a = scan.nextLine();
        while (a.equalsIgnoreCase("y")) {
            MonetaryCoin o = new MonetaryCoin();
            int c;
            c = o.face();
            System.out.println("面值为:"+c);
            sum += c;
            System.out.println("是否继续:");
            a = scan.nextLine();
        }
        System.out.println("总和：" + sum);
        MonetaryCoin o = new MonetaryCoin();
        o.flip();
        boolean n = o.isHeads();
        System.out.println("抛一枚硬币是否为正面："+ n);
    }
}
