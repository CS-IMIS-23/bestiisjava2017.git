package seven_week;

public class SchoolBook2 extends Book2{
     private int age;
     private String str;

     //构造函数,a是页数，b是年龄(4~16岁)
    public SchoolBook2 (int a,int b) {
        super(a);
        age = b;
    }
    public String level() {
        if (age <=6) {
            str = "Pre-school.";
            return str;
        }
        else
            if (age <= 9)
            {
                str = "Early";
                return str;
            }
            else {
                if (age <= 12) {
                    str = "Middle";
                    return str;
                } else {
                    str = "Upper";
                    return str;
                }
            }
    }
}
