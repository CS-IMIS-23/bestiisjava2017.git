package The_three_week;

import The_two_week.EmptyCollectionException;

public class LinkedQueue<T> implements QueueADT {
    private int count;
    private LinearNode<T> head,tail,front;

    public LinkedQueue() {
        count = 0;
        head = tail = null;
    }

    @Override
    public void enqueue(Object element) {
        LinearNode<T> node = new LinearNode<T>((T) element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        front = head;
        T result = front.getElement();
        head = head.getNext();
        count--;

        if(isEmpty())
            tail = null;

        return result;
    }

    @Override
    public T first() {
        T result = head.getElement();
        return result;
    }

    @Override
    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }
    public String toString(){
        LinearNode<T> node ;
        String res = "";
        node = head;


        while (node != null) {
            res += node.getElement()+" ";
            node = node.getNext();
        }
        return res;
    }
}
