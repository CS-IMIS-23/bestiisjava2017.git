package The_three_week;

import The_two_week.EmptyCollectionException;

public class CircularArrayQueue<T> implements QueueADT<T> {
private final int DEFAULT_CAPACITY = 100;
private int front, rear, count;
private T[] queue;

public CircularArrayQueue(int initialCapacity){
        front = rear =count = 0;
        queue = ((T[])(new Object[initialCapacity]));

        }

public CircularArrayQueue(){
        queue = ((T[])(new Object[DEFAULT_CAPACITY]));
        }

@Override
public void enqueue(T element) {
        if(size() == queue.length)
        expandCapacity();
        }

private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length*2]);
        for(int scan = 0;scan <count; scan++){
        larger[scan] = queue[front];
        front = (front + 1) % queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
        }


@Override
public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
        throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;
        return result;

        }

@Override
public T first() {
        return null;
        }

@Override
public boolean isEmpty() {
        return false;
        }

@Override
public int size() {
        return 0;
        }
        }