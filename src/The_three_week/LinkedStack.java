package The_three_week;

import The_two_week.EmptyCollectionException;
import The_two_week.StackADT;

public class LinkedStack<T> implements StackADT<T> {
    private int count;
    private LinearNode<T> top;


    public LinkedStack() {
        count = 0;
        top = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp= new LinearNode<T>(element);
        temp.setNext(top);
        top = temp;
        count ++;
    }

    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Empty!");
        }
        T result = top.getElement();
        top=top.getNext();
        count--;
        return result;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new EmptyCollectionException("Empty!");
        }
        T result = top.getElement();
        return result;
    }

    @Override
    public boolean isEmpty() {
        boolean p ;
        if (size()==0){
            p=true;
        }
        else
        {
            p=false;
        }
        return p;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toSting() {
        LinearNode<T> node ;
        String res = "";
        node = top;


        while (node != null) {
            res += node.getElement()+" ";
            node = node.getNext();
        }
        return res;
    }
}
