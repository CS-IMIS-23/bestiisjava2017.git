package The_three_week;

public class LQTester {
    public static void main(String[] args) {
        LinkedQueue lq = new LinkedQueue();
        System.out.println("Has an empty queue? " + lq.isEmpty());
        lq.enqueue(2);
        lq.enqueue(5);
        lq.enqueue("+");
        lq.enqueue(6);
        lq.enqueue(7);
        System.out.println("queue:" + lq.toString());
        System.out.println("size:" + lq.size());
        System.out.println("first:" + lq.first());
        System.out.println("dequeue:" + lq.dequeue());
        System.out.println("size:" + lq.size());
        System.out.println("toString:" + lq.toString());

        System.out.println("Has an empty queue? " + lq.isEmpty());

    }
}
