package The_three_week;

public class AQTester {
    public static void main(String[] args) {
        ArrayQueue aq = new ArrayQueue();
        System.out.println("Has an empty queue? " + aq.isEmpty());
        aq.enqueue(2);
        aq.enqueue(4);
        aq.enqueue("-");
        aq.enqueue(5);
        System.out.println("queue:" + aq.toString());
        System.out.println("size:" + aq.size());
        System.out.println("first:" + aq.first());
        System.out.println("dequeue:" + aq.dequeue());
        System.out.println("size:" + aq.size());
        System.out.println("toString:" + aq.toString());
        System.out.println("Has an empty queue? " + aq.isEmpty());
    }
}
