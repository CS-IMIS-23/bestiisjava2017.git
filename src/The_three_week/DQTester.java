package The_three_week;

public class DQTester {
    public static void main(String[] args) {
        Deque dq = new Deque();
        System.out.println("Has an empty queue? " + dq.isEmpty());

        dq.enqueueTail(10);
        dq.enqueueTail(8);
        dq.enqueueTail("*");
        dq.enqueueHead(2);
        dq.enqueueHead(4);
        dq.enqueueHead("-");
        dq.enqueueHead(5);

        System.out.println("queue:" + dq.toString());
        System.out.println("size:" + dq.size());
        System.out.println("firstHead:" + dq.firstHead());
        System.out.println("dequeueHead:" + dq.dequeueHead());
        System.out.println("toString:" + dq.toString());
        System.out.println("firstTail:" + dq.firstTail());
        System.out.println("dequeueTail:" + dq.dequeueTail());
        System.out.println("toString:" + dq.toString());
        System.out.println("size:" + dq.size());
        System.out.println("Has an empty queue? " + dq.isEmpty());
    }
}
