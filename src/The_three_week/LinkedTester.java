package The_three_week;

public class LinkedTester {
    public static void main(String[] args) {
        LinkedStack ls = new LinkedStack();
        System.out.println("Has element? "+ls.isEmpty());
        ls.push(2);
        ls.push(5);
        ls.push("+");
        ls.push(6);
        ls.push(7);
        System.out.println("stack:"+ ls.toSting());
        System.out.println("size:"+ ls.size());
        System.out.println("peek:"+ls.peek());
        System.out.println("pop:"+ls.pop());
        System.out.println("size:"+ ls.size());
        System.out.println("toString:"+ ls.toSting());

        System.out.println("Has element? "+ls.isEmpty());

    }
}
