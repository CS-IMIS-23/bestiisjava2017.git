package The_three_week;

import The_two_week.EmptyCollectionException;

public class Deque<T> {
    private int count;
    private LinearNode<T> head, tail, front,previous,temp;

    public Deque() {
        count = 0;
        head = tail = null;
    }

    //从尾部加入
    public void enqueueTail(Object element) {
        LinearNode<T> node = new LinearNode<T>((T) element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }
    //从头部加入
    public void enqueueHead(Object element) {
        LinearNode<T> node = new LinearNode<T>((T) element);

        if (head == null) {
            head = node;
            tail = node;
        } else {
            node.setNext(head);
            head = node;
        }
        count++;
    }
    //从头部删除
    public T dequeueHead() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        front = head;
        T result = front.getElement();
        head = head.getNext();
        count--;

        if (isEmpty())
            tail = null;

        return result;
    }
    //从尾部删除
    public T dequeueTail() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        front = tail;
        temp = head;
        T result = front.getElement();
        while(temp.getNext() != tail){
            previous = temp;
            temp = temp.getNext();
        }
        temp.setNext(null);
        count--;
        return result;


    }

    //查看头部元素
    public T firstHead() {
        T result = head.getElement();
        return result;
    }

    //查看尾部元素
    public T firstTail() {
        T result = tail.getElement();
        return result;
    }


    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }

    public int size() {
        return count;
    }

    public String toString() {
        LinearNode<T> node;
        String res = "";
        node = head;

        while (node != null) {
            res += node.getElement() + " ";
            node = node.getNext();
        }
        return res;
    }
}
