package The_three_week;

import The_two_week.EmptyCollectionException;

import java.util.Arrays;

public class ArrayQueue<T> {
    private final int DEFAULT_CAPACITY = 100;

    private int rear;
    private T[] queue;

    public ArrayQueue() {
        rear = 0;
        queue = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public void enqueue(T element) {
        if (size() == queue.length)
            expandCapacity();
        queue[rear] = element;
        rear++;
    }

    private void expandCapacity() {
        queue = Arrays.copyOf(queue, queue.length * 2);
    }

    public T dequeue() throws EmptyCollectionException {
        if (queue[0] == null)
            throw new EmptyCollectionException("Stack");

        T result = queue[0];
        for (int i = 0;i<rear;i++)
        queue[i] = queue[i+1];
        rear--;
        return result;
    }

    public boolean isEmpty() {
        if (rear == 0)
            return true;

        else
            return false;

    }

    public T first() {
        T a = queue[0];
        return a;
    }

    public int size() {
        return rear;
    }

    public String toString() {
        String result = "";
        for (int b = 0; b < size(); b++)
            result += queue[b] + " ";
        return result;
    }
}

