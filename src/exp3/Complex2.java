package exp3;

public class Complex2 {

    double RealPart;
    double ImagePart;

    public Complex2(double r, double I) {
        RealPart  =r;
        ImagePart =I;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart){
        ImagePart = imagePart;
    }

    public boolean equals(Complex2 op2){
        if (RealPart == op2.RealPart && ImagePart ==  op2.ImagePart)
            return true;
        else
            return false;

    }

    public String toString(){
        String complex = null;
        if(ImagePart>0)
            complex = RealPart + "+" + ImagePart + "i";
        if (ImagePart == 0)
            complex = RealPart + "";
        if (ImagePart < 0)
            complex = RealPart + "" + ImagePart + "i";
        return complex;
    }

    public Complex2 ComplexAdd(Complex2 op2){

        return new Complex2(RealPart + op2.RealPart,ImagePart +  op2.ImagePart);
    }

    public Complex2 ComplexSub(Complex2 op2){

        return new Complex2(RealPart - op2.RealPart,ImagePart -  op2.ImagePart);
    }

    public Complex2 ComplexMulti(Complex2 op2){

        return new Complex2(RealPart * op2.RealPart,ImagePart *  op2.ImagePart);
    }

    public Complex2 ComplexDev(Complex2 op2){
        double a,b,c,d;
        a = RealPart;b=ImagePart;c=op2.RealPart;d=op2.ImagePart;

        return new Complex2((a*c + b*d)/(c*c+d*d),(b*c - a*d)/(c*c +d*d ));
    }

}
