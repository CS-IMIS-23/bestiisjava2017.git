package First_exp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class LinkedListTest2 {
    public static void main(String[] args) {
        String str = "1 2"; // 要写入的内容
        Scanner scan = new Scanner(System.in);
        String res;
        LinkedQueue lq = new LinkedQueue();
        System.out.println("Enter:");
        res = scan.nextLine();
        StringTokenizer st = new StringTokenizer(res);

        while (st.hasMoreTokens()) {
            lq.enqueue(st.nextToken());
        }

        System.out.println(lq.toString());
        System.out.println("sum:" + lq.size());

        try {
            FileOutputStream out = new FileOutputStream("E:/ReadWriteshuchu.txt"); // 输出文件路径
            out.write(str.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream in = new FileInputStream("E:/ReadWriteshuchu.txt"); // 读取文件路径
            byte bs[] = new byte[in.available()];
            in.read(bs);
            res = new String(bs);
            StringTokenizer sz = new StringTokenizer(res);
            while (sz.hasMoreTokens()) {
                if (sz.nextToken().equalsIgnoreCase("1")) {
                    lq.insert(1, 5);
                    break;
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("插入1：" + lq.toString());
        System.out.println("sum:" + lq.size());
        try {
            FileInputStream in = new FileInputStream("E:/ReadWriteshuchu.txt"); // 读取文件路径
            byte bs[] = new byte[in.available()];
            in.read(bs);
            res = new String(bs);
            StringTokenizer sz = new StringTokenizer(res);
            while (sz.hasMoreTokens()) {
                if (sz.nextToken().equalsIgnoreCase("2")) {
                    lq.insert(2, 0);
                    break;
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("插入2：" + lq.toString());
        System.out.println("sum:" + lq.size());

        lq.delete(5);
        System.out.println("删除1：" + lq.toString());
        System.out.println("sum:" + lq.size());
        System.out.println("排序：" +lq.SelectionSort(lq));
    }
}
