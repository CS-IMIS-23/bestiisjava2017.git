package First_exp;

import java.util.Scanner;
import java.util.StringTokenizer;

public class LinkedListTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String res;
        LinkedQueue lq = new LinkedQueue();
        System.out.println("Enter:");
        res = scan.nextLine();
        StringTokenizer st = new StringTokenizer(res);

        while (st.hasMoreTokens()){
           lq.enqueue(st.nextToken());
        }

        System.out.println(lq.toString());
        System.out.println("sum:"+lq.size());

    }
}
