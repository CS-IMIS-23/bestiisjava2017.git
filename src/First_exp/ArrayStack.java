package First_exp;

import The_two_week.EmptyCollectionException;

import java.util.Arrays;


public class ArrayStack<T> {
    private final int DEFAULT_CAPACITY = 100;

    private int nyxy;
    private T[] stack;

    public ArrayStack() {
        nyxy = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public void push(T element) {
        if (size() == stack.length)
            expandCapacity();
        stack[nyxy] = element;
        nyxy++;
    }

    private void expandCapacity() {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }

    public T pop() throws EmptyCollectionException {
        if (stack[0] == null)
            throw new EmptyCollectionException("Stack");
        nyxy--;
        T result = stack[nyxy];
        stack[nyxy] = null;

        return result;
    }

    public boolean isEmpty() {
        if (nyxy == 0) {
            System.out.println("It is empty stack.");
            return true;
        } else {
            System.out.println("It has element.");
            return false;
        }
    }

    public T peek() {
        T a = null;
        if (stack[0] != null) {
            a = stack[nyxy - 1];
        }
        return a;
    }

    public int size() {
        return nyxy;
    }

    public String toString() {
        String result = "";
        for (int b = 0; b < size(); b++)
            result += stack[b] + " ";
        return result;
    }

    public void insert(T elem, int a) {
        if (a == 0) {
            for (int i = nyxy + 1; i > 0; i--) {
                stack[i] = stack[i - 1];
            }
            stack[0] = elem;
        } else {
            int i;
            for (i = nyxy + 1; i > a - 1; i--) {
                stack[i] = stack[i - 1];
            }
            stack[i] = elem;
        }
        nyxy++;
    }

    //按索引值删除
    public void delete(int a) {
        for (int i = a; i <= nyxy; i++) {
            stack[i] = stack[i + 1];
        }
        nyxy--;
    }

    public void SelectionSort() {
        int n = nyxy;
        for (int i = 0; i < n; i++) {
            int k = i;
            // 找出最小值的小标
            for (int j = i + 1; j < n; j++) {
                if (Integer.parseInt(String.valueOf(stack[j])) < Integer.parseInt(String.valueOf(stack[k]))) {
                    k = j;
                }
            }
            // 将最小值放到排序序列末尾
            if (k > i) {
                T tmp = stack[i];
                stack[i] = stack[k];
                stack[k] = tmp;
                String res = "";
                for (int q = 0; q < nyxy; q++) {
                    res += stack[q] + " ";
                }
                System.out.println(res);
                System.out.println("sum:"+size());
            }
        }
    }
}