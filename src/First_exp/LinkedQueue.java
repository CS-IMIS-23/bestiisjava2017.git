package First_exp;

import The_three_week.QueueADT;
import The_two_week.EmptyCollectionException;

import java.util.Stack;

public class LinkedQueue<T> implements QueueADT {
    private int nyxy;
    private LinearNode<T> head, tail;


    public LinkedQueue() {
        nyxy = 0;
        head = tail = null;
    }

    @Override
    public void enqueue(Object element) {
        LinearNode<T> node = new LinearNode<T>((T) element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        nyxy++;

    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        nyxy--;

        if (isEmpty())
            tail = null;

        return result;
    }

    //按索引值插入
    public void insert(T elem, int a) {
        LinearNode<T> node1 = new LinearNode<T>(elem);
        LinearNode<T> current = head;
        //头插
        if (a == 0) {
            node1.setNext(current);
            head = node1;
        } else {
            for (int i = 1; i < a - 1; i++)
                current = current.getNext();
            node1.setNext(current.getNext());
            current.setNext(node1);
        }
        nyxy++;
    }

    //按索引值删除
    public void delete(int a) {
        LinearNode<T> current, temp = null;
        current = head;

        if (a == 0)
            head = head.getNext();
        else {
            for (int i = 0; i < a; i++) {
                temp = current;
                current = current.getNext();
            }
            temp.setNext(current.getNext());
        }
        nyxy--;
    }

    @Override
    public T first() {
        return head.getElement();
    }

    @Override
    public boolean isEmpty() {
        if (nyxy == 0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return nyxy;
    }

    public String toString() {
        String result = "";
        LinearNode<T> current = head;
        for (int i = 0; i < nyxy; i++) {
            result += current.getElement() + " ";
            current = current.getNext();
        }
        return result;
    }

    public String SelectionSort(LinkedQueue q) {
//        //选择排序法,大的在左
//        int temp;
//        LinearNode current = Head;
//        while (current != null) {
//            LinearNode s = current.getNext();
//            while (s != null) {
//                while (Integer.parseInt(s.getElement()+"") > Integer.parseInt(current.getElement()+"")) {
//                    temp = Integer.parseInt(s.getElement()+"");
//                    s.setElement(current.getElement());
//                    current.setElement(temp);
//                }
//                s = s.getNext();
//            }
//            current=current.getNext();
//            System.out.println();
////            Print(Head);
//            System.out.println();
//            System.out.println("元素总数为：" + size());
//        }
//        return Head;

        int[] a = new int[size()];
        for (int i = 0; i < a.length; i++) {
            a[i] = Integer.valueOf(q.first()+"");
            q.dequeue();
        }
        for (int i = 0; i < a.length - 1; i++) {// 做第i趟排序
            int k = i;
            for (int j = k + 1; j < a.length; j++) {// 选最小的记录
                if (a[j] < a[k]) {
                    k = j; //记下目前找到的最小值所在的位置
                }
            }
            //在内层循环结束，也就是找到本轮循环的最小的数以后，再进行交换
            if (i != k) {  //交换a[i]和a[k]
                int temp = a[i];
                a[i] = a[k];
                a[k] = temp;
            }
            String r = "";
            for(int w = 0;w < a.length;w++)
                r += a[w]+" ";
            System.out.println(r);
            System.out.println("sum:"+a.length);
        }
        String re = "";
        for(int w = 0;w < a.length;w++)
            re += a[w]+" ";
        return re;
    }
}


