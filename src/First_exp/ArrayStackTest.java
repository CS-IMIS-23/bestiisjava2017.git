package First_exp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ArrayStackTest {
    public static void main(String[] args) {
        String str = "1 2";
        Scanner scan = new Scanner(System.in);
        String res;
        ArrayStack as = new ArrayStack();
        System.out.println("Enter:");
        res = scan.nextLine();
        StringTokenizer st = new StringTokenizer(res);

        while (st.hasMoreTokens()) {
            as.push(st.nextToken());
        }

        System.out.println(as.toString());
        System.out.println("sum:" + as.size());
        try {
            FileOutputStream out = new FileOutputStream("E:/ReadWriteshuchu.txt"); // 输出文件路径
            out.write(str.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream in = new FileInputStream("E:/ReadWriteshuchu.txt"); // 读取文件路径
            byte bs[] = new byte[in.available()];
            in.read(bs);
            res = new String(bs);
            StringTokenizer sz = new StringTokenizer(res);
            while (sz.hasMoreTokens()) {
                if (sz.nextToken().equalsIgnoreCase("1")) {
                    as.insert(1, 5);
                    break;
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("插入1：" + as.toString());
        System.out.println("sum:" + as.size());
        try {
            FileInputStream in = new FileInputStream("E:/ReadWriteshuchu.txt"); // 读取文件路径
            byte bs[] = new byte[in.available()];
            in.read(bs);
            res = new String(bs);
            StringTokenizer sz = new StringTokenizer(res);
            while (sz.hasMoreTokens()) {
                if (sz.nextToken().equalsIgnoreCase("2")) {
                    as.insert(2, 0);
                    break;
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("插入2：" + as.toString());
        System.out.println("sum:" + as.size());

        as.delete(5);
        System.out.println("删除1：" + as.toString());
        System.out.println("sum:" + as.size());
        as.SelectionSort();
        System.out.println("排序："+as.toString());
    }
}
