import java.util.Scanner;
import java.text.DecimalFormat;

public class pp3_6
{
	public static void main(String []args)
	{
		double A,V,S;
                
		Scanner scan = new Scanner(System.in);
		
		System.out.print("radius:");
		A = scan.nextDouble();

		V=4/3*Math.PI*Math.pow(A,3);
		S=4*Math.PI*Math.pow(A,2);

		/*格式化*/
		DecimalFormat fmt = new DecimalFormat("0.####");

		System.out.println("V="+fmt.format(V));
		System.out.println("S="+fmt.format(S));
	}
}
