import java.util.Scanner;
import java.text.DecimalFormat;

public class pp3_7
{
	public static void main(String []args)
	{
		double A,B,C,D,S;

		Scanner scan = new Scanner(System.in);

		System.out.print("边1:");
		A = scan.nextDouble();

		System.out.print("边2:");
		B = scan.nextDouble();

		System.out.print("边3:");
		C = scan.nextDouble();

		S =(A+B+C)/2;

		D=Math.sqrt(S*(S-A)*(S-B)*(S-C));

		//格式化
		DecimalFormat fmt = new DecimalFormat("0.000");

		System.out.println("面积:" + fmt.format(D));
	}
}
