import java.util.Random;

public class pp3_9
{
	public static void main(String []args)
	{
		Random generator = new Random();
		int r,h;
		double V,S;

		r = generator.nextInt(20)+1;/*产生随机数为半径*/
		System.out.println("半径:"+r);

		h = generator.nextInt(20)+1;/*产生随机数为高*/
		System.out.println("高:"+h);

		V = Math.PI*Math.pow(r,2)*h;
		S = 2*Math.PI*r*h;

		System.out.println("体积:"+V);
		System.out.println("表面积:"+S);
	}
}
