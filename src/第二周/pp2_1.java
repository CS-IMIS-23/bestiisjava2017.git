//********************************************************
// Lincoln.java      Author:Lewis/Loftus
//
// Demonstrates the basic structure of a java application.
//********************************************************


public class pp2_1
{
	//-------------------------------------------
	//  Prints a presidential quote.
	//-------------------------------------------
	public  static void main(String []args)
	{
		System.out.println("A quote by Abraham Lincoln:");

		System.out.println("\"Whatever you are, be a good one.\"");
	}
}
