//************************************************************************
// Roses.java            AUthor:Lewis/Loftus
//
// Demonstrates the use of escape sequences.
//************************************************************************

public class Roses
{
	//----------------------------------------------------------------
	//  Prints a poem (of sorts) on multiple lines.
	//----------------------------------------------------------------
	public static void main(String []args)
	{
		System.out.println("Roses are red,\n\tViolets are bule,\nSugar is sweet,\n\tBut I have\"commitment issues\",\n\tSo I'd rather just be friends\n\tAt this point in our relationship.");
	}
}
