package test.test; 

import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import test.Complex;

import static org.junit.jupiter.api.Assertions.assertEquals;

/** 
* Complex Tester. 
* 
* @author <Authors name> 
* @since <pre>���� 22, 2018</pre> 
* @version 1.0 
*/ 
public class ComplexTest {
    Complex a = new Complex(1, 2);
    Complex b = new Complex(1, -4);
    Complex c = new Complex(19, 0);
    Complex d = new Complex(0, -3);
    Complex e = new Complex(0, 0);

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: getRealPart()
     */
    @Test
    public void testGetRealPart() throws Exception {
//TODO: Test goes here... 
        assertEquals(1, a.getRealPart());
    }

    @Test
    public void testGetImagePart() throws Exception {
//TODO: Test goes here...
        assertEquals(2, a.getImagePart());
    }
    @Test
    public void testEquals() throws Exception {
//TODO: Test goes here... 
        assertEquals(true, c.equals(19, 0));
        assertEquals(false, d.equals(0, 0));
    }
    @Test
    public void testComplexAdd() throws Exception {
//TODO: Test goes here...
        assertEquals("2.0-2.0i", a.ComplexAdd(b));
    }
    @Test
    public void testComplexSub() throws Exception {
//TODO: Test goes here... 
        assertEquals("0.0+6.0i", a.ComplexSub(b));
    }

    @Test
    public void testComplexMulti() throws Exception {
//TODO: Test goes here...
        assertEquals("9.0-2.0i", a.ComplexMulti(b));
    }
    @Test
    public void testComplexDiv() throws Exception {
//TODO: Test goes here...
        assertEquals("-0.4117647058823529+0.02076124567474049i", a.ComplexDiv(b));
    }
}