package test.test; 

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;

import static org.junit.Assert.assertEquals;

/** 
* Stringb Tester. 
* 
* @author <Authors name> 
* @since <pre>���� 18, 2018</pre> 
* @version 1.0 
*/ 
public class StringbTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: main(String [] args) 
* 
*/ 
@Test
public void testMain() throws Exception { 
//TODO: Test goes here... 
}
    StringBuffer a = new StringBuffer("StringScanne"); // 测试12个字符（<=16）
    StringBuffer b = new StringBuffer("StringScanneStringScanne"); // 测试24个字符（>16&&<=34）
    StringBuffer c = new StringBuffer("StringScanneStringScanneStringScanne"); // 测试36个字符（=>34）

    @Test
    public void testcharAt() throws Exception {
        assertEquals('S', a.charAt(0));
        assertEquals('g', b.charAt(5));
        assertEquals('a', a.charAt(8));
    }

    @Test
    public void testcapacity() throws Exception {
        assertEquals(28, a.capacity());
        assertEquals(40, b.capacity());
        assertEquals(52, c.capacity());
    }

    @Test
    public void testlength() throws Exception {
        assertEquals(12, a.length());
        assertEquals(24, b.length());
        assertEquals(36, c.length());
    }

    @Test
    public void testindexOf() throws Exception {
        assertEquals(0, a.indexOf("Str"));
        assertEquals(6, b.indexOf("Sca"));
        assertEquals(0, c.indexOf("Str"));
    }


} 
