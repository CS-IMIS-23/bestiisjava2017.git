package test.exp3; 

import exp3.Complex2;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import static org.junit.jupiter.api.Assertions.assertEquals;

/** **
* Complex2 Tester. 
* 
* @author <Authors name> 
* @since <pre>���� 16, 2018</pre> 
* @version 1.0 
*/ 
public class Complex2Test {
    Complex2 num1 = new Complex2(4, 1);
    Complex2 num2 = new Complex2(2, 1);
    Complex2 num3 = new Complex2(1,2);
    Complex2 num4 = new Complex2(2,2);

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getRealPart() 
* 
*/ 
@Test
public void testGetRealPart() throws Exception { 
//TODO: Test goes here...
} 

/** 
* 
* Method: getImagePart() 
* 
*/ 
@Test
public void testGetImagePart() throws Exception { 
//TODO: Test goes here...
} 

/** 
* 
* Method: setRealPart(double realPart) 
* 
*/ 
@Test
public void testSetRealPart() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setImagePart(double imagePart) 
* 
*/ 
@Test
public void testSetImagePart() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: equals(Complex2 op2) 
* 
*/ 
@Test
public void testEquals() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: toString() 
* 
*/ 
@Test
public void testToString() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: ComplexAdd(Complex2 op2) 
* 
*/ 
@Test
public void testComplexAdd() throws Exception { 
//TODO: Test goes here...
    assertEquals("6.0+2.0i",(num1.ComplexAdd(num2).toString()));
    assertEquals("3.0+3.0i",(num2.ComplexAdd(num3).toString()));
    assertEquals("6.0+3.0i",(num1.ComplexAdd(num4).toString()));
    assertEquals("4.0+3.0i",(num2.ComplexAdd(num4).toString()));
} 

/** 
* 
* Method: ComplexSub(Complex2 op2) 
* 
*/ 
@Test
public void testComplexSub() throws Exception { 
//TODO: Test goes here...
    assertEquals("2.0",num1.ComplexSub(num2).toString());
} 

/** 
* 
* Method: ComplexMulti(Complex2 op2) 
* 
*/ 
@Test
public void testComplexMulti() throws Exception { 
//TODO: Test goes here...
    assertEquals("8.0+1.0i",num1.ComplexMulti(num2).toString());
} 

/** 
* 
* Method: ComplexDev(Complex2 op2) 
* 
*/ 
@Test
public void testComplexDev() throws Exception { 
//TODO: Test goes here...
    assertEquals("1.8-0.4i",num1.ComplexDev(num2).toString());
} 


} 
