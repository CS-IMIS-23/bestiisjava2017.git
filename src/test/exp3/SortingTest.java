package test.exp3;

import Third_exp.Sorting;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class SortingTest {

    @Test
    public void selectionSort() throws Exception{
        Comparable[] ar = {0,5,9,14,2332,23,32};
        assertEquals("0,5,9,14,23,32,2332,",Sorting.selectionSort(ar));
        Comparable[] ar1 = {"1","3","23","32"};
        assertEquals("1,23,3,32,",Sorting.selectionSort(ar1));
        Comparable[] ar2 = {"32","23","3","1"};
        assertEquals("1,23,3,32,",Sorting.selectionSort(ar2));
        Comparable[] ar22 = {"32","23","3","1"};
        assertNotEquals("32,23,3,1,",Sorting.selectionSort(ar22));
        Comparable[] ar3 = {0,5,9,14};
        assertEquals("0,5,9,14,",Sorting.selectionSort(ar3));
        Comparable[] ar4 = {2332,32,23};
        assertEquals("23,32,2332,",Sorting.selectionSort(ar4));
        Comparable[] ar5 = {"A","B","C","D"};
        assertEquals("A,B,C,D,",Sorting.selectionSort(ar5));
        Comparable[] ar6 = {"D","C","B","A"};
        assertEquals("A,B,C,D,",Sorting.selectionSort(ar6));
        Comparable[] ar7 = {"A","D","C","S"};
        assertEquals("A,C,D,S,",Sorting.selectionSort(ar7));
        Comparable[] ar8 = {"1","3","A","C"};
        assertEquals("1,3,A,C,",Sorting.selectionSort(ar8));
        Comparable[] ar9 = {"c","3","a","d"};
        assertEquals("3,a,c,d,",Sorting.selectionSort(ar9));

    }

    @Test
    public void insertionSort() {
    }

    @Test
    public void bubbleSort() {
    }

    @Test
    public void mergeSort() {
    }

    @Test
    public void quickSort() {
    }
}