package test.exp3;


import Third_exp.Searching;

import static org.junit.Assert.assertEquals;

public class SearchingTest {

    @org.junit.Test
    public void linearSearch() throws Exception{
        Comparable[] ar = {0,5,9,14,2332,23,32};
        assertEquals(true,Searching.linearSearch(ar,0,6,0));
        assertEquals(true,Searching.linearSearch(ar,0,6,2332));
        assertEquals(true,Searching.linearSearch(ar,0,6,32));
        assertEquals(false,Searching.linearSearch(ar,0,6,90));

        Comparable[] ar1 = {"1","3","23","32"};
        assertEquals(true,Searching.linearSearch(ar1,0,3,"1"));
        assertEquals(true,Searching.linearSearch(ar1,0,3,"32"));
        assertEquals(false,Searching.linearSearch(ar1,0,3,"5"));

        Comparable[] ar2 = {"32","23","3","1"};
        assertEquals(true,Searching.linearSearch(ar2,0,3,"1"));
        assertEquals(true,Searching.linearSearch(ar2,0,3,"32"));
        assertEquals(false,Searching.linearSearch(ar2,0,3,"33"));

        Comparable[] ar3 = {"AY","EY","QY","CY"};
        assertEquals(true,Searching.linearSearch(ar3,0,3,"AY"));
        assertEquals(true,Searching.linearSearch(ar3,0,3,"CY"));
        assertEquals(false,Searching.linearSearch(ar3,0,3,"ZY"));

    }

    @org.junit.Test
    public void binarySearch() {
//        Comparable[] ar = {0,5,9,14,2332,23,32};
//        assertEquals(true,Searching.binarySearch(ar,0,6,0));
//        assertEquals(true,Searching.binarySearch(ar,0,6,2332));
//        assertEquals(true,Searching.binarySearch(ar,0,6,5));
//        assertEquals(false,Searching.binarySearch(ar,0,6,2));
//
//        Comparable[] ar1 = {"1","3","23","32"};
//        assertEquals(true,Searching.binarySearch(ar1,0,3,"1"));
//        assertEquals(true,Searching.binarySearch(ar1,0,3,"23"));
//        assertEquals(false,Searching.binarySearch(ar1,0,3,"5"));
//
//        Comparable[] ar2 = {"32","23","3","1"};
//        assertEquals(true,Searching.binarySearch(ar2,0,3,"1"));
//        assertEquals(true,Searching.binarySearch(ar2,0,3,"23"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"33"));
//
//        Comparable[] ar3 = {"ABCD","EFGH","QWER","ZXCV"};
//        assertEquals(true,Searching.binarySearch(ar3,0,3,"ABCD"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"CHDG"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"ZASD"));
    }
}