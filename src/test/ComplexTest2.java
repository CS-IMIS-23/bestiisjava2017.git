package test;

public class ComplexTest2 {
    public static void main(String[] args) {
        Complex a = new Complex(1, 2);
        Complex b = new Complex(1, -4);
        Complex c = new Complex(19, 0);
        Complex d = new Complex(0, -3);
        Complex e = new Complex(0, 0);
        System.out.println("a:"+a);
        System.out.println("b:"+b);
        System.out.println("c:"+c);
        System.out.println("d:"+d);
        System.out.println("e:"+e);
        System.out.println("c与19.0+0.0i相等吗:"+c.equals(19, 0));
        System.out.println("d与0.0+0.0i相等吗:"+d.equals(0, 0));
        System.out.println("("+a.toString()+")+("+b.toString()+")=("+a.ComplexAdd(b)+")");
        System.out.println("("+a+")-("+b+")=("+a.ComplexSub(b)+")");
        System.out.println("("+a+")*("+b+")=("+a.ComplexMulti(b)+")");
        System.out.println("("+a.toString()+")/("+b+")=("+a.ComplexDiv(b)+")");
    }
}
