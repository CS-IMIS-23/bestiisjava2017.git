package test;

public class Complex {
    // 定义属性并生成getter,setter
    double RealPart;
    double ImagePart;

    // 定义构造函数
    public Complex(double R,double I){
        RealPart = R;
        ImagePart = I;
    }
    //设置实部
    public void setRealPart(double a){
        RealPart = a;
    }
    //设置虚部
    public void setImagePart(double a){
        ImagePart = a;
    }
    //获取实部
    public double getRealPart(){
        return RealPart;
    }
    //获取虚部
    public double getImagePart(){
        return ImagePart;
    }

    //Override Object
    public boolean equals(double a,double b){
        if (RealPart == a && ImagePart == b)
            return true;
        else
            return false;
    }
    public String toString(){
        if (ImagePart > 0)
            return RealPart+"+"+ImagePart+"i";
        else if (ImagePart == 0)
            return RealPart+"+"+ImagePart+"i";
        else
            return RealPart+""+ImagePart+"i";

    }

    // 定义公有方法:加减乘除
    public String ComplexAdd(Complex a){
        ImagePart= ImagePart+a.getImagePart();
        RealPart= RealPart+a.getRealPart();
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }
    public String  ComplexSub(Complex a) {
        ImagePart = ImagePart - a.getImagePart();
        RealPart = RealPart - a.getRealPart();
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }

    public String  ComplexMulti(Complex a){
        double R,I;
        R = (RealPart*a.getRealPart())-(ImagePart*a.getImagePart());
        I = (RealPart*a.getImagePart())+(ImagePart*a.getRealPart());
        Complex c = new Complex(R,I);
        return c+"";
    }
    public String  ComplexDiv(Complex a){
        RealPart = (RealPart*a.getRealPart()+a.getImagePart()*ImagePart)/(Math.pow(a.getRealPart(),2)+Math.pow(a.getImagePart(),2));
        ImagePart =(ImagePart*a.getRealPart()-RealPart*a.getImagePart())/(Math.pow(a.getRealPart(),2)+Math.pow(a.getImagePart(),2));
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }
}