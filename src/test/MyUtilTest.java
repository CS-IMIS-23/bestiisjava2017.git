package test;

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;


import static org.junit.jupiter.api.Assertions.assertEquals;

/** 
* MyUtil Tester. 
* 
* @author <Authors name> 
* @since <pre>���� 18, 2018</pre> 
* @version 1.0 
*/ 
public class MyUtilTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: percentage2fivegrade(int grade) 
* 
*/
@org.junit.Test
public void testNormal() {
    assertEquals("不及格", MyUtil.percentage2fivegrade(55));
    assertEquals("及格", MyUtil.percentage2fivegrade(65));
    assertEquals("中等", MyUtil.percentage2fivegrade(75));
    assertEquals("良好", MyUtil.percentage2fivegrade(85));
    assertEquals("优秀", MyUtil.percentage2fivegrade(95));
}

    @org.junit.Test
    public void testExceptions() {
        assertEquals("错误", MyUtil.percentage2fivegrade(105));
        assertEquals("错误", MyUtil.percentage2fivegrade(-55));
    }

    @org.junit.Test
    public void testBoundary() {
        assertEquals("不及格", MyUtil.percentage2fivegrade(0));
        assertEquals("及格", MyUtil.percentage2fivegrade(60));
        assertEquals("中等", MyUtil.percentage2fivegrade(70));
        assertEquals("良好", MyUtil.percentage2fivegrade(80));
        assertEquals("优秀", MyUtil.percentage2fivegrade(90));
        assertEquals("优秀", MyUtil.percentage2fivegrade(100));
    }



} 
