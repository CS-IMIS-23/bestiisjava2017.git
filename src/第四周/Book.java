//*****************************************************************************
//pp4.7
//*****************************************************************************
import java.util.Scanner;
public class Book
{
        Scanner scan = new Scanner(System.in);
	private String name;
	private String author;
	private String press;
	private String copyrightdate;                
        //---------------------------------------------------------------------
        //构造函数。
        //---------------------------------------------------------------------
        public Book()
	{       
		name = "aaa";
		author = "aaa";
                press = "aaa";
		copyrightdate = "aaa";
	}

        //---------------------------------------------------------------------
	//name.
	//---------------------------------------------------------------------
	public void name()
	{
	       name = scan.nextLine();
	}

	//---------------------------------------------------------------------
	//author.
	//---------------------------------------------------------------------
	public void author()
	{
	author = scan.nextLine();
	}

        //---------------------------------------------------------------------
	//press.
	//---------------------------------------------------------------------
	public void press()
	{
		press = scan.nextLine();
	}

	//---------------------------------------------------------------------
	//copyrightdate.
	//---------------------------------------------------------------------
	public void copyrightdate()
	{
	copyrightdate = scan.nextLine();
	}

	public String getName()
	{
		return name;
	}
	
	public String getAuthor()
	{
		return author;
	}

	public String getPress()
	{
		return press;
	}

	public String getCopyrightdate()
	{
		return copyrightdate;
	}

	public String toString()
	{
		return name +"\t"+author+"\t"+press+"\t"+copyrightdate;
	}
}	
