//****************************************************************************
//pp4.7不用Scanner类。
//****************************************************************************
public class Book1
{
	private String name;
	private String author;
	private String press;
	private String copyrightdate;

	//--------------------------------------------------------------------
	//构造函数.
	//--------------------------------------------------------------------
        public Book1(String A,String B,String C,String D)
	{
		name = A;
		author = B;
		press = C;
		copyrightdate = D;
	}
	//--------------------------------------------------------------------
	//name.
	//--------------------------------------------------------------------
	public String getName()
	{
		return name;
	}
	//--------------------------------------------------------------------
	//author.
	//--------------------------------------------------------------------
	public String getAuthor()
	{
		return author;
	}
	//--------------------------------------------------------------------
	//press
	//--------------------------------------------------------------------
	public String getPress()
	{
		return press;
	}
	//--------------------------------------------------------------------
	//copyrightdate.
	//--------------------------------------------------------------------
	public String getCopyrightdate()
	{
		return copyrightdate;
	}
	public String setName(String name1)
	{
		name = name1;
		return name;
	}
	public String setAuthor(String author1)
	{
		author = author1;
		return author;
	}
	public String setPress(String press1)
	{
		press = press1;
		return press;
	}
	public String setCopyrightdate(String date)
	{
		copyrightdate = date;
		return copyrightdate;
	}
	public String toString()
	{
		return name + "\t" +author + "\t" + press + "\t" + copyrightdate;
	}
}
