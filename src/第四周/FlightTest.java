//pp4.8
public class FlightTest
{
	public static void main(String []args)
	{
		Flight flight = new Flight();

		System.out.println("航线名:"+flight.getName());
		System.out.println("航班号:"+flight.getNum());
		System.out.println("始发地:"+flight.getPlace1());
		System.out.println("目的地:"+flight.getPlace2());
		System.out.println("概况:"+flight.toString());
		
		System.out.println("航线名:"+flight.setName("马航"));
		System.out.println("航班号:"+flight.setNum(7));
		System.out.println("始发地:"+flight.setPlace1("中国"));
		System.out.println("目的地:"+flight.setPlace2("马来西亚"));
		System.out.println("概况:"+flight.toString());
	}
}
