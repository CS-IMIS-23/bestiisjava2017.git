//pp4.6
public class Box
{
	private double high,weigh,thick;
	boolean full;
	//构造方法
	public Box()
	{
		high = 0;
		weigh = 0;
		thick = 0;
		boolean full=false;
	}
	public double getHigh()
	{
		return high;
	}
	public double getWeigh()
	{
		return weigh;
	}
	public double getThick()
	{
		return thick;
	}
	public boolean getFull()
	{
		return full;
	}
	public double setHigh(double high1)
	{
		high = high1;
		return high;
	}
	public double setWeigh(double weigh1)
	{
		weigh = weigh1;
		return weigh;
	}
	public double setThick(double thick1)
	{
		thick = thick1;
		return thick;
	}
	public String toString()
	{
		String result1 = Double.toString(high);
		String result2 = Double.toString(weigh);
		String result3 = Double.toString(thick);
		return "高:"+result1+"\t宽:"+result2+"\t厚:"+result3;
	}
	public Boolean isfull(double num)
	{
		boolean full=num>=high*weigh*thick;
		return full;
	}
}
