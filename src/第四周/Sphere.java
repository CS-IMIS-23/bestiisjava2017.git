//pp4.3
public class Sphere
{
	private double d,v,s;
	//构造方法
	public Sphere()
	{
		d = 0;
	}
	//设置直径
	public double getSphere()
	{
		d = 3.14;
		return d;
	}
	//获取直径
	public double setSphere(double num)
	{
		d = num;
		return d;
	}
	//体积
	public double V()
	{
		v = Math.PI*Math.pow((d/2),3)*(4/3);
		return v;
	}
	//表面积
	public double S()
	{
		s = 4*Math.PI*Math.pow((d/2),2);
		return s;
	}
	//toString
	public String toString()
	{
		String result1 = Double.toString(d);
		String result2 = Double.toString(v);
		String result3 = Double.toString(s);
		return "直径:" + result1 + "\t" + "体积:" + result2 + "\t" + "表面积:" + result3;
	}

}
