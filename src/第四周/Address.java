//****************************************************************************************************
//Address.java    Author:Lewis/Loftus
//
//Represents a street address.
//****************************************************************************************************

public class Address
{
	private String streetAddress,city,state;
	private long zipCode;

	//--------------------------------------------------------------------------------------------
	//Constructor:Set up this address with the sprcified data.
	//--------------------------------------------------------------------------------------------
	public Address(String street,String town,String st,long zip)
	{
		streetAddress = street;
		city = town;
		state = st;
		zipCode = zip;
	}

	//--------------------------------------------------------------------------------------------
	//Returns a description of this Address object.
	//--------------------------------------------------------------------------------------------
	public String toString()
	{
		String result;

		result = streetAddress + "\n";
		result += city + "," + state + " " + zipCode;
		return result;
	}
}
