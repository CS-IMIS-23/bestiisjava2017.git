//pp4.1
public class CounterTest
{
	public static void main(String []args)
	{
		Counter num1 = new Counter();
		Counter num2 = new Counter();

		System.out.println("初始值:"+num1.getCount());
		System.out.println("初始值:"+num2.getCount());
		num2.click();
		System.out.println("+1值:"+num2.getCount());
		System.out.println("返回值:"+num2.reset());
		num1.click();
		System.out.println("+1值:"+num1.getCount());
		num1.click();
		System.out.println("再加1值:"+num1.getCount());
		System.out.println("返回值:"+num1.reset());
		
	}
}
