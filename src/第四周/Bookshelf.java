public class Bookshelf
{
	public static void main(String []args)
	{
		Book book = new Book();

		System.out.print("name:");
		book.name();

		System.out.println();
		System.out.print("author:");
		book.author();

		System.out.println();
		System.out.print("press:");
		book.press();

	        System.out.println();  
		System.out.print("copyrightdate:");
		book.copyrightdate();

		System.out.println(book.toString());
	}
}
