public class Bookshelf1
{
	public static void main(String []args)
	{
		Book1 book = new Book1("Java程序设计教程","Lewis","电子工业出版社","2018.2.28");
		
		System.out.println("name:"+book.getName());
		System.out.println("author:"+book.getAuthor());
		System.out.println("press:"+book.getPress());
		System.out.println("copyrightdate:"+book.getCopyrightdate());
		System.out.println(book);

		System.out.println("name"+book.setName("高等数学"));
		System.out.println("author:"+book.setAuthor("同济大学"));
		System.out.println("press:"+book.setPress("高等教育出版社"));
		System.out.println("copyrightdate:"+book.setCopyrightdate("2018.3.4"));
		System.out.println(book);
		}
}
