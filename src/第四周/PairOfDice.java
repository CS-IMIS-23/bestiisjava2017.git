//pp4.9
public class PairOfDice
{
	private final int MAX=6;
	private int value1;
	private int value2;
	
	//构造方法
	public PairOfDice()
	{
		value1 = 1;
		value2 = 2;
	}
	//获取骰子
	public int getValue1()
	{
		return value1;
	}
	public int getValue2()
	{
		return value2;
	}
	//设置骰子
	public int setValue1(int num1)
	{
		value1 = num1;
		return num1;
	}
	public int setValue2(int num2)
	{
		value2 = num2;
		return num2;
	}
	//掷骰子
	public void roll()
	{
		value1 = (int)(Math.random()*MAX)+1;
		value2 = (int)(Math.random()*MAX)+1;
	}
	//获取值
	public int getValuesum()
	{
		int sum;
		sum = value1 + value2;
		return sum;
	}
}
