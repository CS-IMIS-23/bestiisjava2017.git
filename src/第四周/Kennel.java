//pp4.4
public class Kennel
{
	public static void main(String []args)
	{
		Dog dog1 = new Dog();
		Dog dog2 = new Dog();

		System.out.println("名:"+dog1.getName());
		System.out.println("年龄:"+dog1.getAge());
		System.out.println("相当于人的年龄:"+dog1.peopleAge());
		System.out.println("概况:"+dog1.toString());
		System.out.println("名:"+dog2.setName("Nancy"));
		System.out.println("年龄:"+dog2.setAge(7));
		System.out.println("相当于人的年龄:"+dog2.peopleAge());
		System.out.println("概况:"+dog2.toString());
	}
}
