//pp4.5
public class CarTest
{
	public static void main(String []args)
	{
		Car car = new Car();
		//get.
		System.out.println(car.getName());
		System.out.println(car.getNum());
		System.out.println(car.getDate());
		System.out.println(car.toString());
		System.out.println(car.isAntique());
		//set.
		System.out.println(car.setName("宝马"));
		System.out.println(car.setNum("x5"));
		System.out.println(car.setDate(1949));
		System.out.println(car.toString());
		System.out.println(car.isAntique());
	}
}
