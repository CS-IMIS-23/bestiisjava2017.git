//pp4.6
public class BoxTest
{
	public static void main(String []args)
	{
		Box box = new Box();
		System.out.println("高:"+box.getHigh());
		System.out.println("宽:"+box.getWeigh());
		System.out.println("厚:"+box.getThick());
		System.out.println("概况:"+box.toString());
		System.out.println("是否装满:"+box.getFull());
		System.out.println("高:"+box.setHigh(5.2));
		System.out.println("宽:"+box.setWeigh(8));
		System.out.println("厚:"+box.setThick(2));
		System.out.println("概况:"+box.toString());
		System.out.println("5是否装满:"+box.isfull(5));
		System.out.println("100是否装满:"+box.isfull(100));
	}
}
