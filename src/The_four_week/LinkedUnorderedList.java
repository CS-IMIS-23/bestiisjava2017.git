package The_four_week;

public class LinkedUnorderedList<T> extends LinkedList<T>
        implements UnorderedListADT<T>
{
    public LinkedUnorderedList()
    {
        super();
    }

    @Override
    public void addToFront(T element)
    {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    @Override
    public void addToRear(T element)
    {
        LinearNode node = new LinearNode(element);

        if (isEmpty()){
            head = node;
        }
        else {
            tail.setNext(node);
        }
        tail = node;
        count++;
    }


    @Override
    public void addAfter(T element, T target)
    {
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();

        for (int i = 0;i < count - 1;i++){
            if (target.equals(current.getElement())) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }
}
