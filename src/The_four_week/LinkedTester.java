package The_four_week;

public class LinkedTester {
    public static void main(String[] args) {
        System.out.println("有序列表:");
        LinkedOrderedList orderedList = new LinkedOrderedList();
        orderedList.add("5");
        orderedList.add("4");
        orderedList.add("2");
        orderedList.add("1");

        System.out.println(orderedList.toString());
        System.out.println(orderedList.isEmpty());
        System.out.println(orderedList.size());
        System.out.println(orderedList.first());
        System.out.println(orderedList.last());
        System.out.println(orderedList.contains("1"));
        orderedList.removeLast();
        System.out.println(orderedList.toString());

        System.out.println("无序列表:");
        LinkedUnorderedList unorderedList = new LinkedUnorderedList();
        unorderedList.addToFront("9");
        unorderedList.addToFront("8");
        unorderedList.addToFront("5");
        unorderedList.addToRear("4");
        unorderedList.addAfter("0","5");
        System.out.println(unorderedList.toString());
        unorderedList.remove("9");
        System.out.println(unorderedList.toString());
        unorderedList.removeFirst();
        System.out.println(unorderedList.toString());
        unorderedList.removeLast();
        System.out.println(unorderedList.toString());
    }
}
