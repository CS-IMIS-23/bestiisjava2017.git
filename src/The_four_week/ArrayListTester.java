package The_four_week;

public class ArrayListTester {
    public static void main(String[] args) {
        System.out.println("有序列表：");
        ArrayOrderedList orderedList = new ArrayOrderedList();
        orderedList.add("1");
        orderedList.add("2");
        orderedList.add("3");
        orderedList.add("5");
        System.out.println(orderedList.toString());
        System.out.println(orderedList.first());
        System.out.println(orderedList.last());
        System.out.println(orderedList.isEmpty());
        System.out.println(orderedList.contains("3"));
        orderedList.remove("5");
        System.out.println(orderedList.toString());


        System.out.println("无序列表：");
        ArrayUnorderedList unorderedList = new ArrayUnorderedList();
        unorderedList.addToFront("5");
        unorderedList.addToFront("4");
        unorderedList.addToFront("3");
        unorderedList.addToRear("2");
        unorderedList.addAfter("1","3");
        System.out.println(unorderedList.toString());
        System.out.println(unorderedList.size());
        System.out.println(unorderedList.last());
        unorderedList.remove("5");
        System.out.println(unorderedList.toString());
        unorderedList.removeFirst();
        System.out.println(unorderedList.toString());
        unorderedList.removeLast();
        System.out.println(unorderedList.toString());
    }
}
