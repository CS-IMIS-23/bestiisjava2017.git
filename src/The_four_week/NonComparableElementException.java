package The_four_week;

public class NonComparableElementException extends RuntimeException
{
    public NonComparableElementException (String collection)
    {
        super ("The " + collection + " requires comparable elements.");
    }
}
