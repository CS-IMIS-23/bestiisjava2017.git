package The_four_week;

import java.util.Scanner;

public class YangHuiTriangle {
    public static void main(String[] args) {
        CircularArrayQueue queue = new CircularArrayQueue();
        Scanner scan = new Scanner(System.in);
        int a,b=0,c;

        System.out.println("需要显示的行数：");
        int n = scan.nextInt();

        queue.enqueue(0);
        queue.enqueue(1);
        for (int i = 0;i<=n;i++)
            System.out.print(" ");
        System.out.println(1);
        for(int times = 1;times < n;times++){
            queue.enqueue(0);
            for(int e = n-times; e>=0;e--){
                System.out.print(" ");
            }

            for (int row = 1;row <= queue.size()-1;row++) {

                if(queue.size()==1) {
                    a = b;
                    b = (int)queue.first();
                    c = a + b;
                    queue.enqueue(c);
                    System.out.print(c+" ");
                }
                else {
                    a = (int) queue.dequeue();
                    b = (int) queue.first();
                    c = a + b;
                    queue.enqueue(c);
                    System.out.print(c+ " ");
                }

            }
            System.out.println();
        }
    }
}
