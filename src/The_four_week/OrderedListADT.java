package The_four_week;

public interface OrderedListADT<T> extends ListADT<T>
{

    public void add(T element);
}