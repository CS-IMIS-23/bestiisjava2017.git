//This is a comment on a single line.

//-----------------------------------------------------
//Some comments such as those above methods or classes
//deserve to be blocked off to focus special attention
//on a particular aspect of your code.Note that each of
//these lines is technically a separate comment.
//-----------------------------------------------------

/*
  This is one comment
  that spans several lines.
*/
public class Hello
{
	public static void main(String []args)
	{
		System.out.println("Hello");
		System.out.println("test");
	}
}
