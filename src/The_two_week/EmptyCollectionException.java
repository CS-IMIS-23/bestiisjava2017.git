package The_two_week;

public class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException(String collection) {
        super("The "+collection+" is empty.");
    }
}
