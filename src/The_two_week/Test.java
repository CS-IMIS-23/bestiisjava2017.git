package The_two_week;

public class Test {
    public static void main(String[] args) {
        ArrayStack ar = new ArrayStack();
        System.out.println("Has element?");
        ar.isEmpty();
        ar.push(2);
        ar.push("+");
        ar.push(6);
        System.out.println("stack(end-top):"+ ar);
        System.out.println("size:"+ ar.size());
        System.out.println("peek:"+ar.peek());
        System.out.println("pop:"+ar.pop());
        System.out.println("size:"+ ar.size());
        System.out.println("Has element?");
        ar.isEmpty();
        System.out.println("toString(end-top):"+ ar.toString());

    }
}
