package The_two_week;

import Ten_week.Magazine;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class LianBiaoTest {
    private Num list;
    ArrayList<Integer> ma = new ArrayList<Integer>();

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public LianBiaoTest() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(int mag) {
        Num node = new Num(mag);
        Num current;
        if (list == null) {
            list = node;
        } else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }


    //在index的位置插入新节点newNum
    public void insert(int index, int newNum) {
        Num node = new Num(newNum);
        Num h1, h2;
        if (index == 0) {
            node.next = list;
            list = node;
        }
        if (index <= size()) {
            int i = 1;
            h1 = list;
            h2 = list;
            while (h2.next != null) {
                h2 = h2.next;
                if (i == index) {
                    node.next = h2;
                    h1.next = node;
                    i++;
                } else {
                    h1 = h2;
                    i++;
                }
            }
        }
        if (index >= size()) {
            add(newNum);
        }
    }

    public int size() {
        Num head = list;
        int res = 0;
        if (head != null) {
            res++;
        }
        while (head.next != null) {
            head = head.next;
            res++;
        }
        return res;
    }

    //删除节点delNode
    public void delete(int delNode) {
        Num node = new Num(delNode);
        Num h1, h2;
        if (node.number == list.number) {
            list = list.next;
        } else {
            h1 = list;
            h2 = list;
            while (h2.next != null) {
                h2 = h2.next;
                if (h2.number == node.number) {
                    if (h2.next != null)
                        h1.next = h2.next;
                    else
                        h1.next = null;
                } else
                    h1 = h1.next;
            }
            if (h2.number == node.number && (h2.next == null)) {
                h1.next = null;
            }
        }
    }


    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        Num current = list;

        while (current != null) {
            result += current.number + " ";
            current = current.next;
        }

        return result;
    }


    //使用冒泡排序法进行排序
    public Num Sort(Num head) {
        Num node = head, current = null;
        //当链表为空或仅有一个结点时
        if (head == null || head.next == null) {
            return head;
        }
        while (node.next != current) {
            while (node.next != current) {
                if (node.number > node.next.number) {
                    int temp = node.number;
                    node.number = node.next.number;
                    node.next.number = temp;
                }
                node = node.next;
            }
            current = node;//使下一次遍历的尾结点为当前结点
            node = head;
        }
        return head;
    }
}