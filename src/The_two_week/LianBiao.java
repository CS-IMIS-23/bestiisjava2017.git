package The_two_week;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;


public class LianBiao {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        LianBiaoTest lb = new LianBiaoTest();

        String res;
        int a ,b = 1;
        System.out.println("Please enter a number:");
        a = scan.nextInt();
        lb.add(a);

        System.out.println("y/n?");
        res = scan.next();
        while (res.equals("y")) {
            System.out.println("Please enter a number:");
            a = scan.nextInt();
            lb.add(a);
            b++;
            System.out.println("y/n?");
            res = scan.next();
        }
        System.out.println("未插入："+lb.toString());
        lb.add(2);
        System.out.println("插入后(尾插)："+lb.toString());
        lb.insert(0,2);
        System.out.println("插入后(头插):"+lb.toString());
        lb.insert(3,10);
        System.out.println("插入后(中插):"+lb.toString());
        lb.delete(2);
        System.out.println("删除后："+lb.toString());

        StringTokenizer st = new StringTokenizer(lb.toString());
        Num n = new Num(Integer.parseInt(st.nextToken()));
        Num no = n;
        while (st.hasMoreTokens()){
            while (no != null) {
                no.next = new Num(Integer.parseInt(st.nextToken()));
                no = no.next;
            }
        }
        lb.Sort(no);
        System.out.println(lb.Sort(no).toString());
        System.out.println(lb.toString());
    }
}
