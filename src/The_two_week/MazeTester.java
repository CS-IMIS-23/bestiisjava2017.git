package The_two_week;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class MazeTester {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the name of the file containing the maze:");
        String filename = scanner.nextLine() ;

        Maze labyrinth = new Maze(filename);

        System.out.println(labyrinth);

        MazeSolver solver = new MazeSolver(labyrinth);
        if(solver .traverse() ) {
            System.out.println("the maze was successfully traversed");
        } else {
            System.out.println("there is no possible path.");
        }

        System.out.println(labyrinth);
    }
}
