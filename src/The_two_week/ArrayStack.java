package The_two_week;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.EmptyStackException;


public class ArrayStack<T> {
    private final int DEFAULT_CAPACITY = 100;

    private int top;
    private T[] stack;

    public ArrayStack() {
        top = 0;
        stack = (T[])(new Object[DEFAULT_CAPACITY]);
    }

    public void push(T element){
        if (size() == stack.length)
            expandCapacity();
        stack[top] = element;
        top++;
    }
    private void expandCapacity(){
        stack = Arrays.copyOf(stack,stack.length*2);
    }

    public T pop() throws EmptyCollectionException{
        if(stack[0]== null)
            throw new EmptyCollectionException("Stack");
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }

    public boolean isEmpty() {
        if(top==0) {
            System.out.println("It is empty stack.");
            return true;
        }
        else{
            System.out.println("It has element.");
            return false;
        }
    }

    public T peek(){
        T a = null;
        if(stack[0]!= null){
            a = stack[top-1];
        }
        return a;
    }

    public int size(){
        return top;
    }

    public String toString(){
        String result ="";
        for(int b = 0 ;b < size();b++)
            result += stack[b] +" ";
        return result;
    }
}
