public class Cow extends Animal {
    private String a, b, c;

    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        a = "牛吃草。";

    }
    @Override
    public void sleep() {
        b = "牛卧着睡。";
    }

    @Override
    public void introduction() {
        c = "牛（拉丁学名：Bovine），属牛族，为牛亚科下的一个族.";
    }

    public String getEat() {
        return a;
    }

    public String getSleep() {
        return b;
    }

    public String getIntroduction() {
        return c;
    }
}