package The_six_week;

import The_four_week.ElementNotFoundException;
import The_four_week.ListADT;
import The_two_week.EmptyCollectionException;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class ArrayList<T> implements ListADT<T>, Iterable<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;
    protected T[] list;
    protected int modCount;


    public ArrayList()
    {
        this(DEFAULT_CAPACITY);
    }


    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);
        modCount = 0;
    }


    protected void expandCapacity()
    {
        list = Arrays.copyOf(list,list.length * 2);
    }



    public T removeLast() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result = list[rear];
        list[rear] = null;
        rear--;

        return result;
    }

    public T removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result = list[0];
        for (int i = 0; i < rear - 1;i++){
            list[i] = list[i + 1];
        }

        list[rear - 1] = null;
        rear--;

        return result;
    }

    public T remove(T element)
    {
        T result;
        int index = find(element);

        if (index == NOT_FOUND) {
            throw new ElementNotFoundException("ArrayList");
        }

        result = list[index];
        rear--;

        for (int scan=index; scan < rear; scan++) {
            list[scan] = list[scan+1];
        }

        list[rear] = null;
        modCount++;

        return result;
    }

    public T first() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }
        return list[0];
    }


    public T last() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }
        return list[rear - 1];
    }


    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }


    private int find(T target)
    {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty()) {
            while (result == NOT_FOUND && scan < rear) {
                if (target.equals(list[scan])) {
                    result = scan;
                }
                else {
                    scan++;
                }
            }
        }
        return result;
    }


    public boolean isEmpty()
    {
        if (size() == 0){
            return true;
        }
        else {
            return false;
        }
    }


    public int size()
    {
        return rear;
    }


    public String toString()
    {
        String str = "";
        for (int i = 0;i < rear;i++){
            str += list[i] + " ";
        }

        return str;
    }


    public Iterator<T> iterator()
    {
        return new ArrayListIterator();
    }


    private class ArrayListIterator implements Iterator<T>
    {
        int iteratorModCount;
        int current;


        public ArrayListIterator()
        {
            iteratorModCount = modCount;
            current = 0;
        }


        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (iteratorModCount != modCount) {
                throw new ConcurrentModificationException();
            }

            return (current < rear);
        }


        @Override
        public T next() throws ConcurrentModificationException
        {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            current++;

            return list[current - 1];
        }

        @Override
        public void remove() throws UnsupportedOperationException
        {
            throw new UnsupportedOperationException();
        }

    }
}