package The_six_week;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree a = new LinkedBinaryTree(1);
        LinkedBinaryTree b = new LinkedBinaryTree(3);
        LinkedBinaryTree c = new LinkedBinaryTree(8, a, b);
        LinkedBinaryTree d = new LinkedBinaryTree(10);
        LinkedBinaryTree e = new LinkedBinaryTree(24);
        LinkedBinaryTree f = new LinkedBinaryTree(5, d, e);
        LinkedBinaryTree g = new LinkedBinaryTree(0, c, f);
        System.out.println("前序：");
        g.preOrder(g.root);
        System.out.println();
        System.out.println("中序：");
        g.inOrder(g.root);
        System.out.println();
        System.out.println("后序：");
        g.postOrder(g.root);
        System.out.println();
        System.out.println("层序：");
        g.levekOrder();
        System.out.println();
        System.out.println("树状：");
        System.out.println(g.printTree());
        System.out.println();
        System.out.println("size: "+g.size());
        System.out.println("height: "+g.getHeight());
        System.out.println("是否包含8："+g.contains(8));
        System.out.println("叶结点为true，内部结点为false,10为什么结点？: "+g.isWhat(d));
        System.out.println("叶结点为true，内部结点为false，8为什么结点？: "+g.isWhat(c));
        g.removeRightSubtree(c.root,g.root);
        System.out.println("removeRightSubtree:");
        g.levekOrder();
        g.removeAllElements(g);
        System.out.println();
        System.out.println("removeAllElements:");
        g.levekOrder();

    }
}
