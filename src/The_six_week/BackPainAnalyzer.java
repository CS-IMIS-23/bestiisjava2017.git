package The_six_week;

import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("sort");
        System.out.println("CountLefe:" + expert.getCountLeaf());
        System.out.println("Deep:" + expert.getDeep());
        System.out.println("非层序遍历：");
        expert.levekOrder();
        System.out.println("层序遍历：");
        expert.getLevekOrder();


//        expert.evaluate();
    }
}
