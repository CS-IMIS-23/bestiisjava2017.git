package The_six_week;



public class NonComparableElementException extends RuntimeException
{

    public NonComparableElementException (String collection)
    {
        super ("The " + collection + " requires comparable elements.");
    }
}
