
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws Exception {
        //1.建立客户端Socket连接，指定服务器位置和端口
//        Socket socket = new Socket("localhost",8080);
        Socket socket = new Socket("localhost",8080);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
 //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        Scanner scan = new Scanner(System.in);
        String expression,poxtfix="";
        System.out.println("Please enter infix：");
        expression = scan.nextLine();/*中缀*/

        MyDC dc = new MyDC();
        List<String> A = dc.InfixToPostfix(dc.work(expression));
        for (int i = 0 ;i< A.size();i++)
            poxtfix += A.get(i)+" ";
        System.out.println("poxtfix：" + poxtfix);/*后缀*/
//        String info = new String(poxtfix.getBytes("GBK"),"utf-8");

        //加密
        String s= poxtfix;
        String miwen="";

//        FileInputStream f=new FileInputStream("keykb1.dat");
        KeyAgree ke = new KeyAgree();
//        ObjectInputStream b=new ObjectInputStream(f);
        byte[] de = ke.key();
        SecretKeySpec k=new  SecretKeySpec(de,0,24,"AES");
        Cipher cp=Cipher.getInstance("AES");
        cp.init(Cipher.ENCRYPT_MODE, k);
        byte ptext[]=s.getBytes("UTF8");

        System.out.print("miwen：");
        byte ctext[]=cp.doFinal(ptext);

        for(int i=0;i<ctext.length;i++){
            System.out.print(ctext[i] +" ");
            miwen += ctext[i]+" ";
        }
        System.out.println("");
        //     printWriter.write(info);
        //     printWriter.flush();
        DigestPass dp = new DigestPass();

        String a = dp.MD5(miwen);
        String b = dp.MD5(poxtfix);
        outputStreamWriter.write(miwen+"\n");
        outputStreamWriter.write(a+"\n");
//        outputStreamWriter.write(poxtfix+"\n");
        outputStreamWriter.write(b);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("result:" + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
