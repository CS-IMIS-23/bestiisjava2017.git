package Nine_week;

public class StringTooLongException extends Exception{
    StringTooLongException(String message){
        super(message);
    }
}
