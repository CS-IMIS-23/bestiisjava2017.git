package Nine_week;

import java.util.Scanner;

public class pp11_2 {
    public static void main(String[] args) throws StringTooLongException {
        StringTooLongException problem = new StringTooLongException("Input String is out of 20");
        Scanner scan = new Scanner(System.in);
        String a = "y",b ="";
        System.out.println("请输入(done结束)：");
        while (!a.equalsIgnoreCase("done")){
            System.out.print("");
            a = scan.nextLine();
            b += a;
        }
        try {
            if (b.length() > 20 + 4)//因为算上了done的数，所以要加4
            {
                throw problem;
            } else
                System.out.println(b + "\b\b\b\b");/*排除done*/
        }
        catch(StringTooLongException e){
            System.out.println("已超20个字符");
            System.out.println("之后的字符："+b.substring(20) + "\b\b\b\b");
        }
    }
}
