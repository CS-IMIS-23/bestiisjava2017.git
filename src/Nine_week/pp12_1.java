package Nine_week;

import java.util.Scanner;

public class pp12_1 {
    //-------------------------------------------------------------------------------
    //Tests strings to see if they are palindromes.
    //-------------------------------------------------------------------------------
    public static void main(String[] args) {
        String str, another = "y";
        int left, right;

        Scanner scan = new Scanner(System.in);

        while (another.equalsIgnoreCase("y"))// allows y or Y
        {
            System.out.println("Enter a potential palindrome:");
            str = scan.nextLine();

            left = 0;
            right = str.length() - 1;

            System.out.println();
            if (F(str,left,right) == 0)
                System.out.println("That string is NOT a palindrome.");
            else
                System.out.println("That string IS a palindrome.");

            System.out.println();
            System.out.print("Test another palindrome (y/n)?");
            another = scan.nextLine();
        }
    }

    static int F(String result, int a, int b) {
        int nu1 = result.charAt(a);
        int nu2 = result.charAt(b);
        if (nu1 != nu2 || a > b) {
            return 0;
        }
        if ((nu1 == nu2 && a == (b-1)) || (nu1 == nu2 && a == b)) {
            return 1;
        }
        else {
            return F(result, a + 1, b - 1);
        }
    }
}
