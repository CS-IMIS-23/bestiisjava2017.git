package The_five_week;

public class Searching {

    //线性查找
    public static <T extends Comparable<? super T>> boolean linearSearch(T[] data, int min, int max, T target) {

        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            if (data[index].compareTo(target) == 0)
                found = true;
            index++;
        }

        return found;

    }

    //折半查找
    public static <T extends Comparable<? super T>> boolean binarySearch(T[] data, int min, int max, T target) {
        boolean found = false;
        int mid = (max + min) / 2;

        if (data[mid].compareTo(target) == 0) {
            found = true;
        } else if (data[mid].compareTo(target) > 0) {
            if (min <= mid - 1)
                found = binarySearch(data, min, mid - 1, target);
        } else if (mid + 1 <= max)
            found = binarySearch(data, mid + 1, max, target);

        return found;
    }

    //选择排序
    public static <T extends Comparable<T>>
    void selectionSort(T[] data)
    {
        int min,time = 0;
        T temp;
        long startTime = System.nanoTime();

        for (int index = 0; index < data.length-1; index++)
        {
            time++;
            min = index;
            for (int scan = index+1; scan < data.length; scan++) {
                if (data[scan].compareTo(data[min])<0) {
                    time++;
                    min = scan;
                }
            }
            swap(data, min, index);
        }
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("比较次数： " + time);
        System.out.println("执行时间： " + totalTime + " ns");
    }

//            temp = data[min];
//            data[min] = data[index];
//            data[index] = temp;

    //插入排序
    public static <T extends Comparable<? super T>> void insertionSort(T[] data) {
        int count = 0;
        long startTime = System.nanoTime();
        for (int index = 1; index < data.length; index++) {
            T key = data[index];
            int position = index;
            count++;

            while (position > 0 && data[position - 1].compareTo(key) > 0) {
                data[position] = data[position - 1];
                position--;
                count++;
            }
            data[position] = key;
        }
        long endTime = System.nanoTime();
        System.out.println("程序运行时间：" + (endTime - startTime) + "ns");
        System.out.println("比较次数为：" + count);
    }

    //冒泡排序
    public static <T extends Comparable<? super T>> void bubbleSort(T[] data) {
        int position, scan;
        long startTime = System.nanoTime();
        int count = 0;

        for (position = data.length - 1; position >= 0; position--) {
            count++;
            for (scan = 0; scan <= position - 1; scan++) {
                count++;
                if (data[scan].compareTo(data[scan + 1]) > 0)
                    swap(data, scan, scan + 1);
            }
        }
        long endTime = System.nanoTime();
        System.out.println("程序运行时间：" + (endTime - startTime) + "ns");
        System.out.println("比较次数为：" + count);
    }

    //间隔排序
    public static <T extends Comparable<? super T>> void gapSort(T[] data, int i) {
        int n, scan;

        for (int a = i; a >= 1; a = a - 2) {
            for (n = data.length - 1; n >= 0; n--) {
                for (scan = 0; scan <= data.length - a; scan++) {
                    if (scan + a <= data.length - 1) {
                        if (data[scan].compareTo(data[scan + a]) > 0)
                            swap(data, scan, scan + a);
                    } else {
                        if (scan - a > data.length - 1) {
                            if (data[scan].compareTo(data[scan - a]) > 0)
                                swap(data, scan, scan - a);
                        }
                    }
                }
            }
        }
    }


    //互换
    private static <T extends Comparable<? super T>> void swap(T[] data, int index1, int index2) {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //快速排序
    public static <T extends Comparable<T>> void quickSort(T[] data) {
        long startTime = System.nanoTime();
        quickSort(data, 0, data.length - 1);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("比较次数： " + time1);
        System.out.println("执行时间： " + totalTime + " ns");
    }

    private static int time1 = 0;

    private static <T extends Comparable<T>>
    void quickSort(T[] data, int min, int max) {
        if (min < max) {
            int indexofpartition = partition(data, min, max);

            quickSort(data, min, indexofpartition - 1);

            quickSort(data, indexofpartition + 1, max);
        }
    }
//    public static <T extends Comparable<? super T>> void quickSort(T[] data) {
//        quickSort(data, 0, data.length - 1);
//    }

//    private static <T extends Comparable<? super T>> void quickSort(T[] data, int min, int max) {
//        if (min < max) {
//
//            int indexofpartition = partition(data, min, max);
//
//            quickSort(data, min, indexofpartition - 1);
//            quickSort(data, indexofpartition + 1, max);
//        }
//    }

    private static <T extends Comparable<T>> int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        partitionelement = data[middle];
        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {
            while (left < right && data[left].compareTo(partitionelement) <= 0) {
                left++;
                time1++;
            }

            while (data[right].compareTo(partitionelement) > 0) {
                right--;
                time1++;
            }

            if (left < right) {
                swap(data, left, right);
            }
        }

        swap(data, min, right);

        return right;
    }

    //归并排序
    public static <T extends Comparable<T>>
    void mergeSort(T[] data)
    {
        long startTime = System.nanoTime();
        mergeSort(data, 0, data.length - 1);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("比较次数： " + time);
        System.out.println("执行时间： " + totalTime + " ns");

    }


    private static <T extends Comparable<T>>
    void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
    }

    @SuppressWarnings("unchecked")

    private static int time = 0;
    private static <T extends Comparable<T>>
    void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
                time++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
                time++;
            }
            index++;
        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
            time++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
            time++;
        }

        for (index = first; index <= last; index++) {
            data[index] = temp[index];
        }
    }

//    private static int time = 0;
//
//    public static <T extends Comparable<T>>
//    void mergeSort(T[] data) {
//        long startTime = System.nanoTime();
//        mergeSort(data, 0, data.length - 1);
//        long endTime = System.nanoTime();
//        long totalTime = endTime - startTime;
//        System.out.println("比较次数： " + time);
//        System.out.println("执行时间： " + totalTime + " ns");
//
//    }
//
//    public static <T extends Comparable<? super T>> void mergeSort(T[] data, int min, int max) {
//        if (min < max) {
//            int mid = (min + max) / 2;
//            mergeSort(data, min, mid);
//            mergeSort(data, mid + 1, max);
//            merge(data, min, mid, max);
//        }
//    }
//
//    private static <T extends Comparable<? super T>> void merge(T[] data, int first, int mid, int last) {
//        T[] temp = (T[]) (new Comparable[data.length]);
//
//        int first1 = first, last1 = mid;
//        int first2 = mid + 1, last2 = last;
//        int index = first1;
//
//        while (first1 <= last1 && first2 <= last2) {
//            if (data[first1].compareTo(data[first2]) < 0) {
//                temp[index] = data[first1];
//                first1++;
//                time++;
//            } else {
//                temp[index] = data[first2];
//                first2++;
//                time++;
//            }
//            index++;
//        }
//        while (first1 <= last1) {
//            temp[index] = data[first1];
//            first1++;
//            index++;
//            time++;
//        }
//        while (first2 <= last2) {
//            temp[index] = data[first2];
//            first2++;
//            index++;
//            time++;
//        }
//        for (index = first; index <= last; index++)
//            data[index] = temp[index];
//    }
}
