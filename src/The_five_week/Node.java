package The_five_week;

public class Node<T> {
    private Node next;
    private T element;

    public Node() {
        next = null;
        element = null;
    }

    public Node(T elem) {
        next = null;
        element = elem;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node node) {
        next = node;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T elem) {
        element = elem;
    }

    public String tostring(Node node){
        String res = "";
        while (node!=null) {
            res += node.getElement() + " ";
            node = node.getNext();
        }
        return res;
    }
}
