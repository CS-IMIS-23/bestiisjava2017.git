package The_five_week;

public class Test_one {
    public static void main(String[] args) {
        Integer[] list = {49, 34, 56, 72, 91, 3, 0};
        Integer[] list2 = {49, 34, 56, 72, 91, 3, 0};
        Integer[] list3 = {49, 34, 56, 72, 91, 3, 0};
        Integer[] list4 = {49, 34, 56, 72, 91, 3, 0};
        Integer[] list5 = {49, 34, 56, 72, 91, 3, 0};

        System.out.println("冒泡：");
        Searching.bubbleSort(list);
        for (int num:list)
            System.out.print(num+" ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("插入：");
        Searching.insertionSort(list2);
        for (int num:list2)
            System.out.print(num+" ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("归并：");
        Searching.mergeSort(list3);
        for (int num:list3)
            System.out.print(num+" ");
        System.out.println();
        System.out.println("----------------------------------------------------------");


        System.out.println("快速：");
        Searching.quickSort(list4);
        for (int num:list4)
            System.out.print(num+" ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("选择：");
        Searching.selectionSort(list5);
        for (int num:list5)
            System.out.print(num+" ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

    }
}
