package The_five_week;

import The_five_week.LinearNode;
import The_five_week.LinkedList;
import The_five_week.UnorderedListADT;
import The_two_week.EmptyCollectionException;

public class LinkedUnorderedList<T> extends LinkedList<T>
        implements UnorderedListADT<T> {
    public LinkedUnorderedList() {
        super();
    }

    @Override
    public void addToFront(T element, T money, T date) {
        LinearNode<T> node = new LinearNode<T>(element, money, date);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    @Override
    public void addToRear(T element, T money, T date) {
        LinearNode node = new LinearNode(element, money, date);

        if (isEmpty()) {
            head = node;
        } else {
            tail.setNext(node);
        }
        tail = node;
        count++;
    }


    @Override
    public void addAfter(T element, T money, T date, T target1, T target2, T target3) {
        LinearNode<T> node = new LinearNode<>(element, money, date);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();

        for (int i = 0; i < count - 1; i++) {
            if (target1.equals(current.getName()) && target2.equals(current.price) && target3.equals(current.date)) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }

    public String FoundName(T element) {
        String res = "";
        LinearNode<T> current = head;

        while (current != null) {
            if (element.equals(current.getName())) {
                res += current;
                current = current.getNext();
            } else
                current = current.getNext();
        }
        return res;
    }
    public String FoundPrice(T element) {
        String res = "";
        LinearNode<T> current = head;
        int price = Integer.parseInt((String) element);
        while (current != null) {
            if (price == Integer.parseInt(current.getPrice()+"")) {
                res += current;
                current = current.getNext();
            } else
                current = current.getNext();
        }
        return res;
    }
    public String FoundDate(T element) {
        String res = "";
        LinearNode<T> current = head;

        while (current != null) {
            if (element.equals(current.getDate())) {
                res += current;
                current = current.getNext();
            } else
                current = current.getNext();
        }
        return res;
    }
}
