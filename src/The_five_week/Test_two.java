package The_five_week;

public class Test_two {
    public static void main(String[] args) {
        Integer[] list = {1, 2, 3, 4, 5};
        Integer[] list2 = {1, 2, 3, 4, 5};
        Integer[] list3 = {1, 2, 3, 4, 5};
        Integer[] list4 = {1, 2, 3, 4, 5};
        Integer[] list5 = {1, 2, 3, 4, 5};

        System.out.println("冒泡：");
        Searching.bubbleSort(list);
        for (int num : list)
            System.out.print(num + " ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("插入：");
        Searching.insertionSort(list2);
        for (int num : list2)
            System.out.print(num + " ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("归并：");
        Searching.mergeSort(list3);
        for (int num : list3)
            System.out.print(num + " ");
        System.out.println();
        System.out.println("----------------------------------------------------------");


        System.out.println("快速：");
        Searching.quickSort(list4);
        for (int num : list4)
            System.out.print(num + " ");
        System.out.println();
        System.out.println("----------------------------------------------------------");

        System.out.println("选择：");
        Searching.selectionSort(list5);
        for (int num : list5)
            System.out.print(num + " ");
        System.out.println();
        System.out.println("----------------------------------------------------------");
    }
}
