package The_five_week;


public class Hash {
    private Node[] array; //存储链表的数组
    private int size = 0;//表示集合中存放的对象的数目
    private int key;
    private int count = 0;//记录冲突次数
    private int num;//记录每个数据的比较次数

    public Hash(int length) {
        array = new Node[length]; //创建数组
    }

    public int size() {
        return size;
    }
    public int getCount(){
        return count;
    }
    public int getNum(){
        return num;
    }

    public void add(int value) {
        key = value % 11;//算索引值
        Node newNode = new Node(value); //由value创建一个新节点newNode
        Node node = array[key];//由索引值得到一个节点node

        if (node == null) {
            //若这个是空，则将新节点放入其中
            array[key] = newNode;
            size++;
            System.out.println("index : "+ key + "\t\tdata : " + newNode.tostring(newNode));
        } else {
            //若不为空则遍历这个点上的链表
            Node nextNode = node.getNext();
            count++;

            while (!node.getElement().equals(value) && nextNode != null) {
                node = nextNode;
                count++;
            }
            if (!node.getElement().equals(value)) {
                //若值不相等则加入新节点
                node.setNext(newNode);
                size++;
                System.out.println("index : "+ key + "\t\tdata : " + node.tostring(node));
            }
        }
    }

    public boolean contains(int value) {
        num = 0;
        key = value % 11;
        Node node = array[key];
        //冲突的查找
        while (node != null && !node.getElement().equals(value)) {
            node = node.getNext();
            num++;
        }
        //没有冲突的查找
        if (node != null && node.getElement().equals(value)) {
            num++;
            return true;
        } else {
            //地址的对应元素为空
            return false;
        }
    }
}
