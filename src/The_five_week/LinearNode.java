package The_five_week;

public class LinearNode<T> implements Comparable<LinearNode<T>> {
    private LinearNode<T> next;
    protected T name;
    protected T price;
    protected T date;

    public LinearNode(T name, T price, T date) {
        this.next = null;
        this.name = name;
        this.price = price;
        this.date = date;
    }


    public void setName(T name) {
        this.name = name;
    }

    public T getName() {
        return name;
    }

    public T getPrice() {
        return price;
    }

    public void setPrice(T price) {
        this.price = price;
    }

    public T getDate() {
        return date;
    }

    public void getElement(LinearNode node) {
        name = (T) node.name;
        price = (T) node.price;
        date = (T) node.date;
    }

    public void setDate(T date) {
        this.date = date;
    }

    public LinearNode<T> getNext() {
        return next;
    }

    public void setNext(LinearNode node) {
        next = node;
    }


    public String toString() {
        return "name=" + name + ", price=" + price + ", date=" + date + "\n";
    }

    @Override
    public int compareTo(LinearNode<T> o) {
        String name1 = name + "";
        int price1 = Integer.parseInt(price + "");
        String date1 = date + "";
        if (name1.compareTo(o.getName() + "") < 0) {
            return 1;
        } else {
            if (name1.compareTo(o.getName() + "") > 0) {
                return -1;
            } else {
                if (price1 < Integer.parseInt(o.getPrice() + "")) {
                    return 1;
                } else {
                    if (price1 > Integer.parseInt(o.getPrice() + "")) {
                        return -1;
                    } else {
                        if (date1.compareTo(o.getDate() + "") < 0)
                            return 1;
                        else
                            return -1;
                    }
                }
            }
        }
    }
}