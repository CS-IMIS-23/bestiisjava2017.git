package The_five_week;

import The_four_week.ElementNotFoundException;
import The_four_week.ListADT;
import The_two_week.EmptyCollectionException;

import java.util.*;

public abstract class LinkedList<T> implements ListADT<T>, Iterable<T> {
    protected int count;
    protected LinearNode<T> head, tail;
    protected int modCount;

    public LinkedList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    @Override
    public T removeFirst() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> node = head;
        T result = (T) node;
        head = head.getNext();

        count--;
        return result;
    }

    @Override
    public T removeLast() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> temp = head;

        T result = (T) temp;
        while (temp.getNext() != tail) {
            temp = temp.getNext();
        }
        tail = temp;

        count--;
        return result;
    }

    @Override
    public T remove(T targetElement, T targetElement1, T targetElement2) throws EmptyCollectionException,
            ElementNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        while (current != null && !found) {
            if (targetElement.equals(current.name) && targetElement1.equals(current.price) && targetElement2.equals(current.date)) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }

        if (!found) {
            throw new ElementNotFoundException("LinkedList");
        }

        if (size() == 1) {
            head = tail = null;
        } else if (current.equals(head)) {
            head = current.getNext();
        } else if (current.equals(tail)) {
            tail = previous;
            tail.setNext(null);
        } else {
            previous.setNext(current.getNext());
        }

        count--;
        modCount++;

        return (T) current;
    }

    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        T result = (T) head;
        return result;
    }

    @Override
    public T last() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> temp = head;

        T result = (T) temp;
        while (temp.getNext() != tail) {
            temp = temp.getNext();
        }
        tail = temp;
        return (T) tail;
    }

    @Override
    public boolean contains(T targetElement, T targetElement1, T targetElement2) throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        boolean found = false;

        LinearNode<T> current = head;

        while (current != null && !found) {
            if (targetElement.equals(current.name) && targetElement1.equals(current.price) && targetElement2.equals(current.date)) {
                found = true;
            } else {
                current = current.getNext();
            }
        }

        if (!found) {
            return false;
        } else {
            return true;
        }
    }

    public LinearNode<T> Selection() {
        LinearNode<T> curNode = head;
        while(curNode != null){
            LinearNode<T> nextNode = curNode.getNext();
            while(nextNode != null){
                if(curNode.compareTo(nextNode) == -1){
                    T tempName = curNode.getName();
                    T tempPrice = curNode.getPrice();
                    T tempDate = curNode.getDate();
                    curNode.setName(nextNode.getName());
                    curNode.setPrice(nextNode.getPrice());
                    curNode.setDate(nextNode.getDate());
                    nextNode.setName(tempName);
                    nextNode.setPrice(tempPrice);
                    nextNode.setDate(tempDate);
                }
                nextNode = nextNode.getNext();
            }
            curNode = curNode.getNext();
        }
        return head;

//        while (current != null) {
//            int a = 1, i = 0;
//            LinearNode<T> teq = current;
//            LinearNode<T> min = null, temp = null, teh = null;
//            while (teq.getNext() != null) {
//                if (current.compareTo(teq.getNext()) == 1) {
//                    teq = teq.getNext();
//                } else {
//                    temp = teq.getNext();
//                    teh = temp.getNext();
//                    teq = teq.getNext();
//                }
//            }
//            min = current;
//            min.setNext(current.getNext());
//            System.out.println(min);
//            current.setNext(teh);
//            teq.setNext(current);
//            while (i != a) {
//                current = head;
//                current = current.getNext();
//                i++;
//            }
//            a++;
//        }
//        return head;
    }


    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        LinearNode node = head;
        String result = "";
        int a = count;
        while (a > 0) {
            result += node.toString() + " ";
            node = node.getNext();
            a--;
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }


    private class LinkedListIterator implements Iterator<T> {
        private int iteratorModCount;
        private LinearNode<T> current;

        public LinkedListIterator() {
            current = head;
            iteratorModCount = modCount;
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (iteratorModCount != modCount) {
                throw new ConcurrentModificationException();
            }

            return (current != null);
        }

        @Override
        public T next() throws ConcurrentModificationException {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            T result = (T) current;
            current = current.getNext();
            return result;
        }

        @Override
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }


    }

}


