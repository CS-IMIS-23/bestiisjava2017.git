package six_week;

//********************************************************************
//Student.java            Author:Lewis/Loftus
//
//Represents a college student.
//********************************************************************

public class Student
{
    private String firstName,lastName;
    private Address homeAddress,schoolAddress;
    private int a,b,c,num,av;
    //------------------------------------------------------------
    //Constructor:Sets up this student with the specified values.
    //------------------------------------------------------------
    public Student(String first, String last, Address home, Address school)
    {
        firstName = first;
        lastName = last;
        homeAddress = home;
        schoolAddress = school;
        a = 0;
        b = 0;
        c = 0;
    }
    //------------------------------------------------------------
    //Returns a string description of this Student object.
    //------------------------------------------------------------
    public void setTestScore(int Num,int Grade)
    {
        if (Num == 1)
        {
            num = Num;
            a = Grade;
        }
        else
            if (Num == 2)
            {
                num = Num;
                b = Grade;
            }
            else
            {
                num = Num;
                c = Grade;
            }
    }
    public int getTestScore()
    {
        int n = 0;
        while (num == 1 || num == 2 || num == 3)
        {
            if (num == 1)
            return a;
            else
                if (num == 2)
                    return b;
                else
                    return c;
        }
            return n;
    }
    public int average()
    {
        av = (a+b+c)/3;
        return av;
    }
    public String toString()
    {
        String result;
        result = firstName + " " + lastName + "\n";
        result += "Home Address:\n" + homeAddress + "\n";
        result += "School Address:\n" + schoolAddress + "\n";
        result += "第一次分数:\n" + a + "\n";
        result += "第二次分数:\n" + b + "\n";
        result += "第三次分数:\n" + c + "\n";
        result += "average:\n" + av;

        return result;
    }
}