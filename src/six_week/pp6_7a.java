package six_week;

//*********************************************************************************************************
//Stars.java               Author:Lewis/Loftus
//
//Demonstrates the use of nested for loops.
//*********************************************************************************************************
public class pp6_7a
{
    //---------------------------------------------------------------------------------------
    //Prints a tringle shape using asterisk (star) characters.
    //---------------------------------------------------------------------------------------
    public static void main(String []args)
    {
        final int MAX_ROWS = 10;

        for (int row = 1; row <= MAX_ROWS; row++)
        {
            for (int star = 10; star >= row; star--)
                System.out.print("*");
            System.out.println();
        }
    }
}
