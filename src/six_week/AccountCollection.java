package six_week;

public class AccountCollection {
    Account[] account;

    private int index;
    private boolean result;

    public AccountCollection()
    {
        account = new Account[30];
        index = 0;
    }
    //建户
    public void setNum (String b,long a)
    {
        account[index] = new Account(b,a);
        index++;
    }
    //存款
    public boolean saveMoney(double a,String b,long c)
    {
        boolean result = false;
        for(int i=0;i<account.length;i++){
            if(account[i]!=null){
                if(account[i].getName().equals(b) && account[i].getAcctNumber() == c){
                    account[i].deposit(a);
                    result = true;
                }
            }
        }
        return result;
    }
    //取钱
    public String getMoney(double a,String b,long c)
    {
        String result = "取款失败！";
        for (int i=0;i<account.length;i++) {
            if (account[i] != null) {
                if (account[i].getName().equals(b) && account[i].getAcctNumber() == c && a<=account[i].getBalance()) {
                    account[i].withdraw(a);
                    result = "取款成功！";
                }
            }
        }
        return result;
    }
    //增加利润
    public void addInterest()
    {
        int index = 0;
        double m;
        while (index<30){
            if (account[index]!=null)
            account[index].addInterest();
            index++;
        }
    }
}