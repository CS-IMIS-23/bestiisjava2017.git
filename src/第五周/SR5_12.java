//This is a SR5.12
import java.util.Scanner;
public class SR5_12
{
	public static void main(String []args)
	{
		int temperature;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Temperatur:");
		temperature = scan.nextInt();
		
		if (temperature <= 50)
		{
			System.out.println("It is cool");
			System.out.println("Dress warmly");
		}
		else
			if (temperature >80)
			{
				System.out.println("It is warm");
				System.out.println("Dress coolly");
			}
			else
			{
				System.out.println("It is pleasant");
				System.out.println("Dress pleasantly");
			}
	}
}
