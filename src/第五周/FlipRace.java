//抛两枚硬币
public class FlipRace
{
	public static void main(String []args)
	{
		int a = 0,b = 0,c = 0, d = 0;
		do
		{
			Coin coin1 = new Coin();
			Coin coin2 = new Coin();
			System.out.println("1:"+coin1);
			System.out.println("2:"+coin2);
			if (coin1.isHeads() && coin2.isHeads())
			{
				b++;
				c++;
				a++;
			}
			else
			{
				if (coin1.isHeads() && !coin2.isHeads())
				{
					b++;
					a++;
				}
				else
				{
					if (!coin1.isHeads() && coin2.isHeads())
					{
						c++;
						a++;
					}
					else
					{
						a++;
						b = 0;
						c = 0;
					}
				}
			}
		} while ( b < 3 && c < 3);
		
		if (b == 3 && c == 3)
		{
			System.out.println("不分胜负");
		}
		else
		{
			if (c == 3 && b != 3)
			{
				System.out.println("硬币2赢");
				System.out.println("次数:"+a);
			}
			else
			{
				System.out.println("硬币1赢");
				System.out.println("次数:"+a);
			}
		}
	}
}
