//***************************************************************************************
//PalindromeTester.java               Author:Lewis/Loftus
//
//Demonstrates the use of nested while loops.
//***************************************************************************************

import java.util.Scanner;

public class PalindromeTester
{
	//-------------------------------------------------------------------------------
	//Tests strings to see if they are palindromes.
	//-------------------------------------------------------------------------------
	public static void main(String []args)
	{
		String str, another = "y";
		int left, right;

		Scanner scan = new Scanner(System.in);

		while (another.equalsIgnoreCase("y"))// allows y or Y
		{
			System.out.println("Enter a potential palindrome:");
			str = scan.nextLine();

			String string1 = str.replace(","," ");
			String string2 = string1.replace("."," ");
			String string3 = string2.replace(";"," ");
			String string4 = string3.replace(":"," ");
			String string5 = string4.replace("\'"," ");
			String string6 = string5.replace("\""," ");
			System.out.println("now:" + string6);//去掉标点符号
			String string7 = string6.replace(" ","");
			System.out.println("finished:" + string7);//去掉所有空格
			
			left = 0;
			right = string7.length() - 1;
			String name1 = string7.charAt(left)+"";
			String name2 = string7.charAt(right)+"";
			
			while ((name1.equalsIgnoreCase(name2)) && left < right)
			{
				left++;
				right--;
			}

			System.out.println();
			if (left < right)
				System.out.println("That string is NOT a palindrome.");
			else
				System.out.println("That string IS a palindrome.");

			System.out.println();
			System.out.print("Test another palindrome (y/n)? ");
			another = scan.nextLine();
		}
	}
}
