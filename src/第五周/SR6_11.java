// 数的总和
import java.util.Scanner;

public class SR6_11
{
	public static void main(String []args)
	{
		Scanner scan = new Scanner(System.in);
		int sum = 0,num;

		do
		{
			System.out.println("数字(最后为0):");
			num = scan.nextInt();
			sum += num;
		}
		while  (num != 0);

		System.out.println("总和:"+sum);
	}
}
