package Third_exp;


import static org.junit.Assert.assertEquals;

public class SearchingTest {

    @org.junit.Test
    public void linearSearch() throws Exception {
        Comparable[] ar = {0, 5, 9, 14, 2332, 23, 32};
        assertEquals(true, Searching.linearSearch(ar, 0, 6, 0));
        assertEquals(true, Searching.linearSearch(ar, 0, 6, 2332));
        assertEquals(true, Searching.linearSearch(ar, 0, 6, 32));
        assertEquals(false, Searching.linearSearch(ar, 0, 6, 90));

        Comparable[] ar1 = {"1", "3", "23", "32"};
        assertEquals(true, Searching.linearSearch(ar1, 0, 3, "1"));
        assertEquals(true, Searching.linearSearch(ar1, 0, 3, "32"));
        assertEquals(false, Searching.linearSearch(ar1, 0, 3, "5"));

        Comparable[] ar2 = {"32", "23", "3", "1"};
        assertEquals(true, Searching.linearSearch(ar2, 0, 3, "1"));
        assertEquals(true, Searching.linearSearch(ar2, 0, 3, "32"));
        assertEquals(false, Searching.linearSearch(ar2, 0, 3, "33"));

        Comparable[] ar3 = {"AY", "EY", "QY", "CY"};
        assertEquals(true, Searching.linearSearch(ar3, 0, 3, "AY"));
        assertEquals(true, Searching.linearSearch(ar3, 0, 3, "CY"));
        assertEquals(false, Searching.linearSearch(ar3, 0, 3, "ZY"));

    }

    @org.junit.Test
    public void binarySearch() {
//        Comparable[] ar = {0,5,9,14,2332,23,32};
//        assertEquals(true,Searching.binarySearch(ar,0,6,0));
//        assertEquals(true,Searching.binarySearch(ar,0,6,2332));
//        assertEquals(true,Searching.binarySearch(ar,0,6,5));
//        assertEquals(false,Searching.binarySearch(ar,0,6,2));
//
//        Comparable[] ar1 = {"1","3","23","32"};
//        assertEquals(true,Searching.binarySearch(ar1,0,3,"1"));
//        assertEquals(true,Searching.binarySearch(ar1,0,3,"23"));
//        assertEquals(false,Searching.binarySearch(ar1,0,3,"5"));
//
//        Comparable[] ar2 = {"32","23","3","1"};
//        assertEquals(true,Searching.binarySearch(ar2,0,3,"1"));
//        assertEquals(true,Searching.binarySearch(ar2,0,3,"23"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"33"));
//
//        Comparable[] ar3 = {"ABCD","EFGH","QWER","ZXCV"};
//        assertEquals(true,Searching.binarySearch(ar3,0,3,"ABCD"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"CHDG"));
//        assertEquals(false,Searching.binarySearch(ar2,0,3,"ZASD"));
    }

    @org.junit.Test
    public void InsertionSearch() throws Exception {
        int[] ar = {0, 5, 9, 14, 23, 32, 2332};
        assertEquals(0, Searching.InsertionSearch(ar, 0, 0, 6));
        assertEquals(6, Searching.InsertionSearch(ar, 2332, 0, 6));
        assertEquals(5, Searching.InsertionSearch(ar, 32, 0, 6));
        assertEquals(-1, Searching.InsertionSearch(ar, 90, 0, 6));

    }

    @org.junit.Test
    public void SequenceSearch() throws Exception {
        int[] ar = {0, 5, 9, 23, 32, 2332, 14};
        assertEquals(0, Searching.SequenceSearch(ar, 0, 7));
        assertEquals(5, Searching.SequenceSearch(ar, 2332, 7));
        assertEquals(6, Searching.SequenceSearch(ar, 14, 7));
        assertEquals(-1, Searching.SequenceSearch(ar, 90, 7));

    }

    @org.junit.Test
    public void BinarySearch1() throws Exception {
        int[] ar = {0, 5, 9, 14, 23, 32, 2332};
        assertEquals(0, Searching.BinarySearch1(ar, 0, 7));
        assertEquals(6, Searching.BinarySearch1(ar, 2332, 7));
        assertEquals(3, Searching.BinarySearch1(ar, 14, 7));
        assertEquals(-1, Searching.BinarySearch1(ar, 90, 7));

    }

    @org.junit.Test
    public void BinarySearch2() throws Exception {
        int[] ar = {0, 5, 9, 14, 23, 32, 2332};
        assertEquals(0, Searching.BinarySearch2(ar, 0, 0, 6));
        assertEquals(6, Searching.BinarySearch2(ar, 2332, 0, 6));
        assertEquals(3, Searching.BinarySearch2(ar, 14, 0, 6));
        assertEquals(-1, Searching.BinarySearch2(ar, 90, 0, 6));
    }

    @org.junit.Test
    public void fibonacciSearch() throws Exception {
        int[] ar = {0, 5, 9, 14};
        assertEquals(0, Searching.fibonacciSearch(ar, 0));
        assertEquals(6, Searching.fibonacciSearch(ar, 2332));
        assertEquals(3, Searching.fibonacciSearch(ar, 14));
        assertEquals(-1, Searching.fibonacciSearch(ar, 30));
    }
    @org.junit.Test
    public void treeSearch() throws Exception {
        int[] ar = {0, 5, 9, 14, 23, 32, 2332};
        assertEquals(0, Searching.treeSearch(ar, 0));
        assertEquals(2332, Searching.treeSearch(ar, 2332));
        assertEquals(14, Searching.treeSearch(ar, 14));
//        assertEquals(-1, Searching.treeSearch(ar, 30));
    }
    @org.junit.Test
    public void blocksearch() throws Exception {
        int[] ar={16,12,23,26,32,36,35,95,75};
        int[] index = {23,36,95};
        assertEquals(0, Searching.blocksearch(index,ar,16,3));
        assertEquals(2, Searching.blocksearch(index,ar,23,3));
        assertEquals(8, Searching.blocksearch(index,ar,75,3));
        assertEquals(-1, Searching.blocksearch(index,ar,90,3));

    }
    @org.junit.Test
    public void hashSearch() throws Exception {
        int ar[]={22,12,23,16,32,15};
        assertEquals(22, Searching.hashSearch(ar,22));
        assertEquals(15, Searching.hashSearch(ar, 15));
        assertEquals(-1, Searching.hashSearch(ar, 14));
    }
}