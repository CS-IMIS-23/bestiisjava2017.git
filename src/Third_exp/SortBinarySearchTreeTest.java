package Third_exp;

import The_seven_week.LinkedBinarySearchTree;

public class SortBinarySearchTreeTest {
    public static void main(String[] args) {
        int[] num = {5,1,0,9,7,36,24,15,23,32};
        LinkedBinarySearchTree a = new LinkedBinarySearchTree(num[0]);
        for(int i = 1;i<num.length;i++)
            a.addElement(num[i]);
        a.inOrder(a.root);
        System.out.println();
        Integer[] nu ={5,1,0,9,7,36,24,15,23,32};
        Sorting.quickSort(nu);
        for (int o = 0;o<nu.length;o++)
            System.out.print(nu[o]+" ");
        Sorting.insertionSort(nu);
        for (int o = 0;o<nu.length;o++)
            System.out.print(nu[o]+" ");
        Sorting.selectionSort(nu);
        for (int o = 0;o<nu.length;o++)
            System.out.print(nu[o]+" ");
        Sorting.bubbleSort(nu);
        for (int o = 0;o<nu.length;o++)
            System.out.print(nu[o]+" ");
        Sorting.mergeSort(nu);
        for (int o = 0;o<nu.length;o++)
            System.out.print(nu[o]+" ");
    }
}
