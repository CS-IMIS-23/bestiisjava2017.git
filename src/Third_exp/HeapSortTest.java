package Third_exp;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class HeapSortTest{

    @Test
    public void heapSort() {
        int[] a = {2,4,1,9,3,23,32};
        assertEquals("1 2 3 4 9 23 32 ", HeapSort.HeapSort(a));
    }
}