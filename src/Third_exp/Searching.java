package Third_exp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Searching demonstrates various search algorithms on an array
 * of objects.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class Searching {
    /**
     * Searches the specified array of objects using a linear search
     * algorithm.
     *
     * @param data   the array to be searched
     * @param min    the integer representation of the minimum value
     * @param max    the integer representation of the maximum value
     * @param target the element being searched for
     * @return true if the desired element is found
     */
    //线性查找
    public static <T> boolean linearSearch(T[] data, int min, int max, T target) {
        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }

    /**
     * Searches the specified array of objects using a binary search
     * algorithm.
     *
     * @param data   the array to be searched
     * @param min    the integer representation of the minimum value
     * @param max    the integer representation of the maximum value
     * @param target the element being searched for
     * @return true if the desired element is found
     */
    //二分查找
    public static <T extends Comparable<T>> boolean binarySearch(T[] data, int min, int max, T target) {
        boolean found = false;
        int midpoint = (min + max) / 2;  // determine the midpoint

        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else if (data[midpoint].compareTo(target) > 0) {
            if (min <= midpoint - 1)
                found = binarySearch(data, min, midpoint - 1, target);
        } else if (midpoint + 1 <= max)
            found = binarySearch(data, midpoint + 1, max, target);

        return found;
    }

    //顺序查找
    public static int SequenceSearch(int a[], int value, int n) {
        int i;
        for (i = 0; i < n; i++)
            if (a[i] == value)
                return i;
        return -1;
    }

    //二分查找两个版本
    public static int BinarySearch1(int a[], int value, int n) {
        int low, high, mid;
        low = 0;
        high = n - 1;
        while (low <= high) {
            mid = (low + high) / 2;
            if (a[mid] == value)
                return mid;
            if (a[mid] > value)
                high = mid - 1;
            if (a[mid] < value)
                low = mid + 1;
        }
        return -1;
    }

    //二分查找，递归版本
    public static int BinarySearch2(int a[], int value, int low, int high) {
        if (high < low)
            return -1;
        int mid = low + (high - low) / 2;
        if (a[mid] == value)
            return mid;
        else if (a[mid] > value)
            return BinarySearch2(a, value, low, mid - 1);
        else if (a[mid] < value)
            return BinarySearch2(a, value, mid + 1, high);
        else
            return -1;
    }

    //插值查找
    public static int InsertionSearch(int[] data, int target, int min, int max) {
        if (min == max)
            return -1;
        int mid = min + (target - data[min]) / (data[max] - data[min]) * (max - min);
        if (data[mid] == target)
            return mid;
        else if (data[mid] > target)
            return InsertionSearch(data, target, min, mid - 1);
        else if (data[mid] < target)
            return InsertionSearch(data, target, mid + 1, max);
        else
            return -1;
    }

    //斐波那契查找
    private static int[] fibonacci() {
        int[] f = new int[10];
        f[0] = 0;
        f[1] = 1;
        for (int i = 2; i < f.length; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f;
    }

    public static int fibonacciSearch(int[] data, int key) {
        int low = 0;
        int high = data.length - 1;
        int mid = 0;
        // 斐波那契分割数值下标
        int k = 0;
        // 序列元素个数
        int i = 0;
        // 获取斐波那契数列
        int[] f = fibonacci();
        // 获取斐波那契分割数值下标
        while (data.length > f[k] - 1) {
            k++;
        }

        // 创建临时数组
        int[] temp = new int[f[k] - 1];
        for (int j = 0; j < data.length; j++)
            temp[j] = data[j];

        // 序列补充至f[k]个元素
        // 补充的元素值为最后一个元素的值
        for (i = data.length; i < f[k] - 1; i++) {
            temp[i] = temp[high];
        }

        while (low <= high) {
            // low：起始位置
            // 前半部分有f[k-1]个元素，由于下标从0开始
            // 则-1 获取 黄金分割位置元素的下标
            mid = low + f[k - 1] - 1;

            if (temp[mid] > key) {
                // 查找前半部分，高位指针移动
                // （全部元素） = （前半部分）+（后半部分）
                // f[k] = f[k-1] + f[k-1]
                // 因为前半部分有f[k-1]个元素，所以 k = k-1
                high = mid - 1;
                k = k - 1;

            } else if (temp[mid] < key) {
                // 查找后半部分，高位指针移动
                // （全部元素） = （前半部分）+（后半部分）
                // f[k] = f[k-1] + f[k-1]
                // 因为后半部分有f[k-1]个元素，所以 k = k-2
                low = mid + 1;
                k = k - 2;
            } else {
                // 如果为真则找到相应的位置
                if (mid <= high) {
                    return mid;
                } else {
                    // 出现这种情况是查找到补充的元素
                    // 而补充的元素与high位置的元素一样
                    return high;
                }
            }
        }
        return -1;
    }

    public static int treeSearch(int[] arr, int key) {
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        for (int a = 0; a < arr.length; a++) {
            tree.addElement(arr[a]);
        }
        if (tree.find(key) != null)
            return (int) tree.find(key);
        else
            throw new ElementNotFoundException();
    }

    //分块查找
    //index代表索引数组，st2代表待查找数组，keytype代表要查找的元素，m代表每块大小
    public static int blocksearch(int[] index, int[] st2, int keytype, int m) {
        int i = linearSearch(index, 0, st2.length - 1, keytype);    //shunxunsearch函数返回值为带查找元素在第几块
        if (i >= 0) {
            int j = m * i;   //j为第i块的第一个元素下标
            int curlen = (i + 1) * m;
            while (j < curlen) {
                if (st2[j] == keytype)
                    return j;
                j++;
            }
        }
        return -1;
    }

    public static <T> int linearSearch(int[] data, int min, int max, int target) {
        int index = min;

        if (data[index] >= target)
            return 0;
        int i=1;
        while(i<data.length) {
            if((data[i-1]<target)&&(data[i]>target))
                return i;
            else
                i++;
        }
        return -1;
    }

    //哈希查找
    public static int hashSearch(int data[], int target) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < data.length; i++)
            hashMap.put(Integer.hashCode(data[i]), data[i]);

        int key = Integer.hashCode(target);
        if (hashMap.containsKey(key))
            return hashMap.get(key);

        return -1;//找不到返回-1
    }
}

