import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class classTest {
    public static void main(String[] args) throws IOException {
        FileWriter fileWriter = new FileWriter("递归.txt");
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个整数n：");
        n = scanner.nextInt();
        fileWriter.write("请输入一个整数n："+n+"\n");
        fileWriter.flush();
        fileWriter.write("n=0,则Fn为0；\nn=1,则Fn为1；\nn>=2,则Fn为F(n-1)+F(n-2)。\n");
        fileWriter.flush();
        int result;
        result = Fn(n);
        System.out.println(result);
        fileWriter.write("结果是："+result);
        fileWriter.flush();
        fileWriter.close();
    }
    static int Fn(int n){
        if (n == 0)
            return  0;
        if (n == 1)
            return  1;
        else
            return Fn(n-1)+Fn(n-2);
    }
}
