import java.util.List;
import java.util.Scanner;

public class MyDCTester {

    public static void main(String[] args) {
        String expression, poxtfix="";
        double result;

        //输入表达式
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter infix：");
        expression = scan.nextLine();

        //中缀转后缀
        MyDC dc = new MyDC();
        List<String> A = dc.InfixToPostfix(dc.work(expression));
        for (int i = 0 ;i< A.size();i++)
            poxtfix += A.get(i);
        System.out.println("poxtfix：" + poxtfix);

        //计算结果
        result = dc.doCal(A);
        System.out.println("result:" + result);
    }
}