package HuffmanTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Word {
    char[] chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'
            , 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ','\n'};
    int[] number = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    //读取文件
    public String txtString(File file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
                result.append(System.lineSeparator() + s);
                num(s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
    //得到相应字符出现的次数
    public void num(String string) {
        for (int i = 0; i < 28; i++) {
            int temp = 0;
            for (int j = 0; j < string.length(); j++) {
                if (string.charAt(j) == chars[i])
                    temp++;
            }
            number[i] += temp;
        }
    }
    //得到字符出现次数的数组
    public int[] getNumber() {
        return number;
    }
    //得到字符的数组
    public char[] getChars() {
        return chars;
    }
}