//This is TempConverter.
import java.util.Scanner;

public class pp2_5
{
	public static void main(String []args)	
	{
		final int A=32;	
		final double B=5.0/9.0;
		double C,D;

		Scanner scan=new Scanner(System.in);
		
		System.out.print("华氏温度:");	
		C=scan.nextDouble();	      /*输入华氏温度*/
		
		D=(C-A)*B;	/*（摄氏温度=华氏温度-32）*（5/9）*/
		
		System.out.println("摄氏温度:"+D);	/*转换为摄氏温度并输出结果*/
	}					
}
